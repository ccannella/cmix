#ifndef MIXJUMP_H
#define MIXJUMP_H

#include "MixState.h"

int typicalJumpControl(struct mixState * currentState, struct mixInstruction * instruction);
int JMP(struct mixState * currentState, struct mixInstruction * instruction);
int JSJ(struct mixState * currentState, struct mixInstruction * instruction);
int JOV(struct mixState * currentState, struct mixInstruction * instruction);
int JNOV(struct mixState * currentState, struct mixInstruction * instruction);

int JL(struct mixState * currentState, struct mixInstruction * instruction);
int JE(struct mixState * currentState, struct mixInstruction * instruction);
int JG(struct mixState * currentState, struct mixInstruction * instruction);
int JGE(struct mixState * currentState, struct mixInstruction * instruction);
int JNE(struct mixState * currentState, struct mixInstruction * instruction);
int JLE(struct mixState * currentState, struct mixInstruction * instruction);

int registerJumpControlA(struct mixState * currentState, struct mixInstruction * instruction);
int JAN(struct mixState * currentState, struct mixInstruction * instruction);
int JAZ(struct mixState * currentState, struct mixInstruction * instruction);
int JAP(struct mixState * currentState, struct mixInstruction * instruction);
int JANN(struct mixState * currentState, struct mixInstruction * instruction);
int JANZ(struct mixState * currentState, struct mixInstruction * instruction);
int JANP(struct mixState * currentState, struct mixInstruction * instruction);

int registerJumpControlX(struct mixState * currentState, struct mixInstruction * instruction);
int JXN(struct mixState * currentState, struct mixInstruction * instruction);
int JXZ(struct mixState * currentState, struct mixInstruction * instruction);
int JXP(struct mixState * currentState, struct mixInstruction * instruction);
int JXNN(struct mixState * currentState, struct mixInstruction * instruction);
int JXNZ(struct mixState * currentState, struct mixInstruction * instruction);
int JXNP(struct mixState * currentState, struct mixInstruction * instruction);

int registerJumpControl1(struct mixState * currentState, struct mixInstruction * instruction);
int J1N(struct mixState * currentState, struct mixInstruction * instruction);
int J1Z(struct mixState * currentState, struct mixInstruction * instruction);
int J1P(struct mixState * currentState, struct mixInstruction * instruction);
int J1NN(struct mixState * currentState, struct mixInstruction * instruction);
int J1NZ(struct mixState * currentState, struct mixInstruction * instruction);
int J1NP(struct mixState * currentState, struct mixInstruction * instruction);

int registerJumpControl2(struct mixState * currentState, struct mixInstruction * instruction);
int J2N(struct mixState * currentState, struct mixInstruction * instruction);
int J2Z(struct mixState * currentState, struct mixInstruction * instruction);
int J2P(struct mixState * currentState, struct mixInstruction * instruction);
int J2NN(struct mixState * currentState, struct mixInstruction * instruction);
int J2NZ(struct mixState * currentState, struct mixInstruction * instruction);
int J2NP(struct mixState * currentState, struct mixInstruction * instruction);

int registerJumpControl3(struct mixState * currentState, struct mixInstruction * instruction);
int J3N(struct mixState * currentState, struct mixInstruction * instruction);
int J3Z(struct mixState * currentState, struct mixInstruction * instruction);
int J3P(struct mixState * currentState, struct mixInstruction * instruction);
int J3NN(struct mixState * currentState, struct mixInstruction * instruction);
int J3NZ(struct mixState * currentState, struct mixInstruction * instruction);
int J3NP(struct mixState * currentState, struct mixInstruction * instruction);

int registerJumpControl4(struct mixState * currentState, struct mixInstruction * instruction);
int J4N(struct mixState * currentState, struct mixInstruction * instruction);
int J4Z(struct mixState * currentState, struct mixInstruction * instruction);
int J4P(struct mixState * currentState, struct mixInstruction * instruction);
int J4NN(struct mixState * currentState, struct mixInstruction * instruction);
int J4NZ(struct mixState * currentState, struct mixInstruction * instruction);
int J4NP(struct mixState * currentState, struct mixInstruction * instruction);

int registerJumpControl5(struct mixState * currentState, struct mixInstruction * instruction);
int J5N(struct mixState * currentState, struct mixInstruction * instruction);
int J5Z(struct mixState * currentState, struct mixInstruction * instruction);
int J5P(struct mixState * currentState, struct mixInstruction * instruction);
int J5NN(struct mixState * currentState, struct mixInstruction * instruction);
int J5NZ(struct mixState * currentState, struct mixInstruction * instruction);
int J5NP(struct mixState * currentState, struct mixInstruction * instruction);

int registerJumpControl6(struct mixState * currentState, struct mixInstruction * instruction);
int J6N(struct mixState * currentState, struct mixInstruction * instruction);
int J6Z(struct mixState * currentState, struct mixInstruction * instruction);
int J6P(struct mixState * currentState, struct mixInstruction * instruction);
int J6NN(struct mixState * currentState, struct mixInstruction * instruction);
int J6NZ(struct mixState * currentState, struct mixInstruction * instruction);
int J6NP(struct mixState * currentState, struct mixInstruction * instruction);


#endif
