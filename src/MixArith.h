#ifndef MIXARITH_H
#define MIXARITH_H

#include "MixState.h"

int ADD(struct mixState * currentState, struct mixInstruction * instruction);
int SUB(struct mixState * currentState, struct mixInstruction * instruction);
int MUL(struct mixState * currentState, struct mixInstruction * instruction);
int DIV(struct mixState * currentState, struct mixInstruction * instruction);

#endif 
