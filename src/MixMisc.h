#ifndef MIXMISC_H
#define MIXMISC_H

#include "MixState.h"

int shiftControl(struct mixState * currentState, struct mixInstruction * instruction);
int SLA (struct mixState * currentState, struct mixInstruction * instruction);
int SRA (struct mixState * currentState, struct mixInstruction * instruction);
int SLAX (struct mixState * currentState, struct mixInstruction * instruction);
int SRAX (struct mixState * currentState, struct mixInstruction * instruction);
int SLC (struct mixState * currentState, struct mixInstruction * instruction);
int SRC (struct mixState * currentState, struct mixInstruction * instruction);

int MOVE(struct mixState * currentState, struct mixInstruction * instruction);
int NOP(struct mixState * currentState, struct mixInstruction * instruction);

int specialControl(struct mixState * currentState, struct mixInstruction * instruction);
int HLT(struct mixState * currentState, struct mixInstruction * instruction);
int NUM(struct mixState * currentState, struct mixInstruction * instruction);
int CHAR(struct mixState * currentState, struct mixInstruction * instruction);

#endif
