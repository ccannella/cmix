#include "MixInstruction.h"

struct mixInstruction instructionFromWord(int instructionWord[6]) {
    struct mixInstruction instruction;
    instruction.S = instructionWord[0];
    instruction.A1 = instructionWord[1];
    instruction.A2 = instructionWord[2];
    instruction.I = instructionWord[3];
    instruction.F = instructionWord[4];
    instruction.C = instructionWord[5];
    return instruction;
};