#ifndef CMIX_MIXINSTRUCTION_H
#define CMIX_MIXINSTRUCTION_H

struct mixInstruction{
    int S;
    int A1;
    int A2;
    int I;
    int F;
    int C;
};

struct mixInstruction instructionFromWord(int instructionWord[6]);

#endif //CMIX_MIXINSTRUCTION_H
