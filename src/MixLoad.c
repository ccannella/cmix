#include "MixLoad.h"

int LDA(struct mixState * currentState, struct mixInstruction * instruction){
    loadRegister(currentState, currentState->rA, instruction);
    return 2;
}

int LDX(struct mixState * currentState, struct mixInstruction * instruction){
    loadRegister(currentState, currentState->rX, instruction);
    return 2;
}

int LD1(struct mixState * currentState, struct mixInstruction * instruction){
    int temp[6];
    loadRegister(currentState, temp, instruction);
    if(temp[1] == 0 && temp[2] == 0 && temp[3] == 0){
        currentState->rI[0][0] = temp[0];
        currentState->rI[0][1] = temp[4];
        currentState->rI[0][2] = temp[5];
    }
    return 2;
}

int LD2(struct mixState * currentState, struct mixInstruction * instruction){
    int temp[6];
    loadRegister(currentState, temp, instruction);
    if(temp[1] == 0 && temp[2] == 0 && temp[3] == 0){
        currentState->rI[1][0] = temp[0];
        currentState->rI[1][1] = temp[4];
        currentState->rI[1][2] = temp[5];
    }
    return 2;
}

int LD3(struct mixState * currentState, struct mixInstruction * instruction){
    int temp[6];
    loadRegister(currentState, temp, instruction);
    if(temp[1] == 0 && temp[2] == 0 && temp[3] == 0){
        currentState->rI[2][0] = temp[0];
        currentState->rI[2][1] = temp[4];
        currentState->rI[2][2] = temp[5];
    }
    return 2;
}

int LD4(struct mixState * currentState, struct mixInstruction * instruction){
    int temp[6];
    loadRegister(currentState, temp, instruction);
    if(temp[1] == 0 && temp[2] == 0 && temp[3] == 0){
        currentState->rI[3][0] = temp[0];
        currentState->rI[3][1] = temp[4];
        currentState->rI[3][2] = temp[5];
    }
    return 2;
}

int LD5(struct mixState * currentState, struct mixInstruction * instruction){
    int temp[6];
    loadRegister(currentState, temp, instruction);
    if(temp[1] == 0 && temp[2] == 0 && temp[3] == 0){
        currentState->rI[4][0] = temp[0];
        currentState->rI[4][1] = temp[4];
        currentState->rI[4][2] = temp[5];
    }
    return 2;
}

int LD6(struct mixState * currentState, struct mixInstruction * instruction){
    int temp[6];
    loadRegister(currentState, temp, instruction);
    if(temp[1] == 0 && temp[2] == 0 && temp[3] == 0){
        currentState->rI[5][0] = temp[0];
        currentState->rI[5][1] = temp[4];
        currentState->rI[5][2] = temp[5];
    }
    return 2;
}

int LDAN(struct mixState * currentState, struct mixInstruction * instruction){
    loadRegister(currentState, currentState->rA, instruction);
    if(currentState->rA[0] == 0){
        currentState->rA[0] = 1;
    }
    else{
        currentState->rA[0] = 0;
    }
    return 2;
}

int LDXN(struct mixState * currentState, struct mixInstruction * instruction){
    loadRegister(currentState, currentState->rX, instruction);
    if(currentState->rX[0] == 0){
        currentState->rX[0] = 1;
    }
    else{
        currentState->rX[0] = 0;
    }
    return 2;
}

int LD1N(struct mixState * currentState, struct mixInstruction * instruction){
    int temp[6];
    loadRegister(currentState, temp, instruction);
    if(temp[1] == 0 && temp[2] == 0 && temp[3] == 0){
        currentState->rI[0][0] = temp[0];
        currentState->rI[0][1] = temp[4];
        currentState->rI[0][2] = temp[5];
    }
    if(currentState->rI[0][0] == 0){
        currentState->rI[0][0] = 1;
    }
    else{
        currentState->rI[0][0] = 0;
    }
    return 2;
}

int LD2N(struct mixState * currentState, struct mixInstruction * instruction){
    int temp[6];
    loadRegister(currentState, temp, instruction);
    if(temp[1] == 0 && temp[2] == 0 && temp[3] == 0){
        currentState->rI[1][0] = temp[0];
        currentState->rI[1][1] = temp[4];
        currentState->rI[1][2] = temp[5];
    }
    if(currentState->rI[1][0] == 0){
        currentState->rI[1][0] = 1;
    }
    else{
        currentState->rI[1][0] = 0;
    }
    return 2;
}

int LD3N(struct mixState * currentState, struct mixInstruction * instruction){
    int temp[6];
    loadRegister(currentState, temp, instruction);
    if(temp[1] == 0 && temp[2] == 0 && temp[3] == 0){
        currentState->rI[2][0] = temp[0];
        currentState->rI[2][1] = temp[4];
        currentState->rI[2][2] = temp[5];
    }
    if(currentState->rI[2][0] == 0){
        currentState->rI[2][0] = 1;
    }
    else{
        currentState->rI[2][0] = 0;
    }
    return 2;
}

int LD4N(struct mixState * currentState, struct mixInstruction * instruction){
    int temp[6];
    loadRegister(currentState, temp, instruction);
    if(temp[1] == 0 && temp[2] == 0 && temp[3] == 0){
        currentState->rI[3][0] = temp[0];
        currentState->rI[3][1] = temp[4];
        currentState->rI[3][2] = temp[5];
    }
    if(currentState->rI[3][0] == 0){
        currentState->rI[3][0] = 1;
    }
    else{
        currentState->rI[3][0] = 0;
    }
    return 2;
}

int LD5N(struct mixState * currentState, struct mixInstruction * instruction){
    int temp[6];
    loadRegister(currentState, temp, instruction);
    if(temp[1] == 0 && temp[2] == 0 && temp[3] == 0){
        currentState->rI[4][0] = temp[0];
        currentState->rI[4][1] = temp[4];
        currentState->rI[4][2] = temp[5];
    }
    if(currentState->rI[4][0] == 0){
        currentState->rI[4][0] = 1;
    }
    else{
        currentState->rI[4][0] = 0;
    }
    return 2;
}

int LD6N(struct mixState * currentState, struct mixInstruction * instruction){
    int temp[6];
    loadRegister(currentState, temp, instruction);
    if(temp[1] == 0 && temp[2] == 0 && temp[3] == 0){
        currentState->rI[5][0] = temp[0];
        currentState->rI[5][1] = temp[4];
        currentState->rI[5][2] = temp[5];
    }
    if(currentState->rI[5][0] == 0){
        currentState->rI[5][0] = 1;
    }
    else{
        currentState->rI[5][0] = 0;
    }
    return 2;
}


