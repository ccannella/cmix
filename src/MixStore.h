#ifndef MIXSTORE_H
#define MIXSTORE_H

#include "MixState.h"

int STA(struct mixState * currentState, struct mixInstruction * instruction);
int STX(struct mixState * currentState, struct mixInstruction * instruction);
int ST1(struct mixState * currentState, struct mixInstruction * instruction);
int ST2(struct mixState * currentState, struct mixInstruction * instruction);
int ST3(struct mixState * currentState, struct mixInstruction * instruction);
int ST4(struct mixState * currentState, struct mixInstruction * instruction);
int ST5(struct mixState * currentState, struct mixInstruction * instruction);
int ST6(struct mixState * currentState, struct mixInstruction * instruction);
int STJ(struct mixState * currentState, struct mixInstruction * instruction);
int STZ(struct mixState * currentState, struct mixInstruction * instruction);

#endif
