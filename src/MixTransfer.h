#ifndef MIXTRANSFER_H
#define MIXTRANSFER_H

#include "MixState.h"

int ENTA(struct mixState * currentState, struct mixInstruction * instruction);
int ENTX(struct mixState * currentState, struct mixInstruction * instruction);
int ENT1(struct mixState * currentState, struct mixInstruction * instruction);
int ENT2(struct mixState * currentState, struct mixInstruction * instruction);
int ENT3(struct mixState * currentState, struct mixInstruction * instruction);
int ENT4(struct mixState * currentState, struct mixInstruction * instruction);
int ENT5(struct mixState * currentState, struct mixInstruction * instruction);
int ENT6(struct mixState * currentState, struct mixInstruction * instruction);
int ENNA(struct mixState * currentState, struct mixInstruction * instruction);
int ENNX(struct mixState * currentState, struct mixInstruction * instruction);
int ENN1(struct mixState * currentState, struct mixInstruction * instruction);
int ENN2(struct mixState * currentState, struct mixInstruction * instruction);
int ENN3(struct mixState * currentState, struct mixInstruction * instruction);
int ENN4(struct mixState * currentState, struct mixInstruction * instruction);
int ENN5(struct mixState * currentState, struct mixInstruction * instruction);
int ENN6(struct mixState * currentState, struct mixInstruction * instruction);

int INCA(struct mixState * currentState, struct mixInstruction * instruction);
int INCX(struct mixState * currentState, struct mixInstruction * instruction);
int INC1(struct mixState * currentState, struct mixInstruction * instruction);
int INC2(struct mixState * currentState, struct mixInstruction * instruction);
int INC3(struct mixState * currentState, struct mixInstruction * instruction);
int INC4(struct mixState * currentState, struct mixInstruction * instruction);
int INC5(struct mixState * currentState, struct mixInstruction * instruction);
int INC6(struct mixState * currentState, struct mixInstruction * instruction);

int DECA(struct mixState * currentState, struct mixInstruction * instruction);
int DECX(struct mixState * currentState, struct mixInstruction * instruction);
int DEC1(struct mixState * currentState, struct mixInstruction * instruction);
int DEC2(struct mixState * currentState, struct mixInstruction * instruction);
int DEC3(struct mixState * currentState, struct mixInstruction * instruction);
int DEC4(struct mixState * currentState, struct mixInstruction * instruction);
int DEC5(struct mixState * currentState, struct mixInstruction * instruction);
int DEC6(struct mixState * currentState, struct mixInstruction * instruction);

int addressTransferControlA(struct mixState * currentState, struct mixInstruction * instruction);
int addressTransferControlX(struct mixState * currentState, struct mixInstruction * instruction);
int addressTransferControlI1(struct mixState * currentState, struct mixInstruction * instruction);
int addressTransferControlI2(struct mixState * currentState, struct mixInstruction * instruction);
int addressTransferControlI3(struct mixState * currentState, struct mixInstruction * instruction);
int addressTransferControlI4(struct mixState * currentState, struct mixInstruction * instruction);
int addressTransferControlI5(struct mixState * currentState, struct mixInstruction * instruction);
int addressTransferControlI6(struct mixState * currentState, struct mixInstruction * instruction);

#endif
