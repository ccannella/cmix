#ifndef CMIX_MIXDECODER_H
#define CMIX_MIXDECODER_H

#include <stdlib.h>
#include "MixMisc.h"
#include "MixArith.h"
#include "MixLoad.h"
#include "MixStore.h"
#include "MixIO.h"
#include "MixJump.h"
#include "MixTransfer.h"
#include "MixComp.h"


struct mixDecoder {
    int instructionSetSize;
    int (** instructionSet)(struct mixState *, struct mixInstruction *);
};

void initializeDecoder(struct mixDecoder * decoder, int instructionSetSize, void (*decoderSetup)(int (**instructionSet)(struct mixState *, struct mixInstruction *)));
void standardDecoder(int (**instructionSet)(struct mixState *, struct mixInstruction *));
#endif //CMIX_MIXDECODER_H

