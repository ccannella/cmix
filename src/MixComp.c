#include "MixComp.h"

int CMPA(struct mixState * currentState, struct mixInstruction * instruction){
    registerComparison(currentState, instruction, currentState->rA, 6);
    return 2;
}

int CMPX(struct mixState * currentState, struct mixInstruction * instruction){
    registerComparison(currentState, instruction, currentState->rX, 6);
    return 2;
}

int CMP1(struct mixState * currentState, struct mixInstruction * instruction){
    registerComparison(currentState, instruction, &currentState->rI[0][0], 3);
    return 2;
}

int CMP2(struct mixState * currentState, struct mixInstruction * instruction){
    registerComparison(currentState, instruction, &currentState->rI[1][0], 3);
    return 2;
}

int CMP3(struct mixState * currentState, struct mixInstruction * instruction){
    registerComparison(currentState, instruction, &currentState->rI[2][0], 3);
    return 2;
}

int CMP4(struct mixState * currentState, struct mixInstruction * instruction){
    registerComparison(currentState, instruction, &currentState->rI[3][0], 3);
    return 2;
}

int CMP5(struct mixState * currentState, struct mixInstruction * instruction){
    registerComparison(currentState, instruction, &currentState->rI[4][0], 3);
    return 2;
}

int CMP6(struct mixState * currentState, struct mixInstruction * instruction){
    registerComparison(currentState, instruction, &currentState->rI[5][0], 3);
    return 2;
}

