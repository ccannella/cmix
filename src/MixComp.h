#ifndef MIXCOMP_H
#define MIXCOMP_H

#include "MixState.h"

int CMPA(struct mixState * currentState, struct mixInstruction * instruction);
int CMPX(struct mixState * currentState, struct mixInstruction * instruction);
int CMP1(struct mixState * currentState, struct mixInstruction * instruction);
int CMP2(struct mixState * currentState, struct mixInstruction * instruction);
int CMP3(struct mixState * currentState, struct mixInstruction * instruction);
int CMP4(struct mixState * currentState, struct mixInstruction * instruction);
int CMP5(struct mixState * currentState, struct mixInstruction * instruction);
int CMP6(struct mixState * currentState, struct mixInstruction * instruction);

#endif
