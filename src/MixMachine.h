#ifndef MIXMACHINE_H
#define MIXMACHINE_H

#include "MixState.h"

struct mixMachine {
    int mixBase;
    struct mixState currentState;
    int cadenceSize;
    int currentCadencePosition;
    int * cadence;
};

#endif //MIXMACHINE_H
