#include "MixArith.h"

int ADD(struct mixState * currentState, struct mixInstruction * instruction){
    int temp[6] = {0,0,0,0,0,0};
    int i;
    loadRegister(currentState, temp, instruction);
    if(temp[0] != currentState->rA[0]){
        currentState->rA[0] = (currentState->rA[0] == 0)? 1 :0;
        SUB(currentState, instruction);
        currentState->rA[0] = (currentState->rA[0] == 0)? 1 :0;
    }
    else{
        for(i = 5; i > 1; i--){
            currentState->rA[i] += temp[i];
            if(currentState->rA[i] >= currentState->mixByteStates){
                currentState->rA[i -1] += currentState->rA[i]/currentState->mixByteStates;
                currentState->rA[i] = currentState->rA[i]%currentState->mixByteStates;
            }
        }
        currentState->rA[1] += temp[1];
        if(currentState->rA[1] >= currentState->mixByteStates){
            currentState->rA[1] = currentState->rA[1] % currentState->mixByteStates;
            currentState->overflow = 1;
        }
    }
        
    return 2;
}

int SUB(struct mixState * currentState, struct mixInstruction * instruction){
    int temp[6] = {0,0,0,0,0,0};
    int smaller[6] = {0,0,0,0,0,0};
    int larger[6] = {0,0,0,0,0,0};
    int i, largestValue, largestIndex;
    largestValue = 0;
    largestIndex = 0;
    loadRegister(currentState, temp, instruction);
    for(i = 1; i <6 && largestValue == 0; i++){
        if(currentState->rA[i] >= largestValue){
            largestValue = currentState->rA[i];
            largestIndex = i;
        }
        if(temp[i] > largestValue){
            largestValue = temp[i];
            largestIndex = i;
        }
        else if(temp[i] == largestValue){
            largestValue = 0;
            largestIndex = 0;
        }
    }
    if(currentState->rA[largestIndex] == largestValue){
        for(i = 0; i<6; i++){
            larger[i] = currentState->rA[i];
            smaller[i] = temp[i];
        }
    }
    else{
        for(i = 0; i<6; i++){
            smaller[i] = currentState->rA[i];
            larger[i] = temp[i];
        }
        larger[0] = (larger[0] == 0)? 1:0;
    }
    if(temp[0] != currentState->rA[0]){
        currentState->rA[0] = (currentState->rA[0] == 0)? 1 :0;
        ADD(currentState, instruction);
        currentState->rA[0] = (currentState->rA[0] == 0)? 1 :0;
    }
    else{
        for(i = 1; i< 6; i++){
            larger[i] -= smaller[i];
        }
        for(i = 5; i > 0; i--){
            if(larger[i] < 0){
                larger[i - 1] --;
                larger[i] += currentState->mixByteStates;
            }
        }
        for(i = 0; i < 6; i++){
            currentState->rA[i] = larger[i];
        }
    }
    return 2;
}

int MUL(struct mixState * currentState, struct mixInstruction * instruction){
    int temp[6] = {0,0,0,0,0,0};
    int output[10] = {0,0,0,0,0,0,0,0,0,0};
    int i, j;
    loadRegister(currentState, temp, instruction);
    currentState->rA[0] = (currentState->rA[0] == temp[0])? 0:1;
    currentState->rX[0] = currentState->rA[0];
    for(i = 5; i> 0; i--){
        for(j = 5; j >0; j--){
            output[i + j - 1] += currentState->rA[j]*temp[i];
        }
        for (j = 9; j > 0; j--){
            if(output[j] >= currentState->mixByteStates){
                output[j - 1] += output[j] / currentState->mixByteStates;
                output[j] = output[j] % currentState->mixByteStates;
            }
        }
    }
    for(i = 1; i < 6; i++){
        currentState->rA[i] = output[i - 1];
        currentState->rX[i] = output[i + 4];
    }
    return 10;
}

int DIV(struct mixState * currentState, struct mixInstruction * instruction){
    int numerator[10] = {0,0,0,0,0,0,0,0,0,0};
    int tempNumerator[5] = {0,0,0,0,0};
    int tempDivisor[5] = {0,0,0,0,0};
    int divisor[6] = {0,0,0,0,0,0};
    int quotient[5] = {0,0,0,0,0};
    int remainder[5] = {0,0,0,0,0};
    int i, j, numeratorLength, divisorLength, quotientGuess, lowerGuess, carry, nextQuotient;
    int numeratorPosition;
    loadRegister(currentState, divisor, instruction);
    numeratorLength = 0;
    divisorLength = 0;
    for(i = 0; i <5; i++){
        numerator[i] = currentState->rA[i + 1];
        numerator[i+5] = currentState->rX[i+1];
        if(divisor[i+1] != 0 && divisorLength == 0){
            divisorLength = 5-i;
        }
    }
    for(i = 0; i < 10; i++){
        if(numerator[i] != 0 && numeratorLength == 0){
            numeratorLength = 10 - i;
        }
    }
    if(divisorLength == 0){
        currentState->overflow = 1;
        return 12;
    }
    numeratorPosition = 10 - numeratorLength;
    carry = 0;
    for(i = numeratorPosition; i < 11 - divisorLength; i++){
        nextQuotient = 0;
        for(j = 0; j<5; j++){
            tempNumerator[4 - j] = (j < divisorLength)? numerator[i + divisorLength - j - 1] : 0;
            tempDivisor[j] = divisor[j + 1];
        }
        tempNumerator[5 - divisorLength] += carry;
        carry = 0;
        quotientGuess = tempNumerator[5-divisorLength] /tempDivisor[5 - divisorLength];
        lowerGuess = tempNumerator[5 - divisorLength] /(tempDivisor[5 - divisorLength] + 1);
        multiplyWord(tempDivisor, 5, currentState->mixByteStates, lowerGuess);
        subtractWords(tempNumerator, tempDivisor, 5, currentState->mixByteStates);
        for(; lowerGuess <= quotientGuess; lowerGuess++){
            if(cmpWord(tempNumerator, &divisor[1], 5) >= 0){
                subtractWords(tempNumerator, &divisor[1], 5, currentState->mixByteStates);
            }
            else{
                break;
            }
        }

        if(tempNumerator[5 - divisorLength] > 0 && i != 10 - divisorLength){
            carry = tempNumerator[5 - divisorLength] * currentState->mixByteStates;
            tempNumerator[5 - divisorLength]= 0;
        }
        for(j = 0; j<5; j++){
            numerator[i + divisorLength - j- 1] = tempNumerator[4 - j];
        }
        nextQuotient = lowerGuess;
        if(quotient [0] != 0){
            currentState->overflow = 1;
            return 12;
        }
        for(j = 0; j < 4; j++){
            quotient[j] = quotient[j + 1];
        }
        quotient[4] = lowerGuess;
    }
    for(i = 0; i < 5; i++){
        currentState->rA[i+1] = quotient[i];
        currentState->rX[i+1] = numerator[i + 5];
    }
    currentState->rX[0]  = currentState->rA[0];
    currentState->rA[0] = (currentState->rA[0] == divisor[0])? 0:1;
    return 12;
}

