#ifndef MIXLOAD_H
#define MIXLOAD_H

#include "MixState.h"

int LDA(struct mixState * currentState, struct mixInstruction * instruction);
int LDX(struct mixState * currentState, struct mixInstruction * instruction);
int LD1(struct mixState * currentState, struct mixInstruction * instruction);
int LD2(struct mixState * currentState, struct mixInstruction * instruction);
int LD3(struct mixState * currentState, struct mixInstruction * instruction);
int LD4(struct mixState * currentState, struct mixInstruction * instruction);
int LD5(struct mixState * currentState, struct mixInstruction * instruction);
int LD6(struct mixState * currentState, struct mixInstruction * instruction);
int LDAN(struct mixState * currentState, struct mixInstruction * instruction);
int LDXN(struct mixState * currentState, struct mixInstruction * instruction);
int LD1N(struct mixState * currentState, struct mixInstruction * instruction);
int LD2N(struct mixState * currentState, struct mixInstruction * instruction);
int LD3N(struct mixState * currentState, struct mixInstruction * instruction);
int LD4N(struct mixState * currentState, struct mixInstruction * instruction);
int LD5N(struct mixState * currentState, struct mixInstruction * instruction);
int LD6N(struct mixState * currentState, struct mixInstruction * instruction);

#endif
