#include "MixIO.h"

int JBUS(struct mixState * currentState, struct mixInstruction * instruction){
    if(currentState->ioPorts[instruction->F].busy) {
        int memoryAddress;
        int J1, J2;
        memoryAddress = computeAddress(currentState, instruction);
        J1 = currentState->nextInstructionAddress / currentState->mixByteStates;
        J2 = currentState->nextInstructionAddress % currentState->mixByteStates;
        currentState->rJ[0] = J1;
        currentState->rJ[1] = J2;
        currentState->nextInstructionAddress = memoryAddress;
    }
    return 1;
}
int IOC(struct mixState * currentState, struct mixInstruction * instruction){
    if (currentState->ioPorts[instruction->F].busy == 0){
        currentState->ioPorts[instruction->F].in = 0;
        currentState->ioPorts[instruction->F].out = 0;
        currentState->ioPorts[instruction->F].ioc = 1;
        currentState->ioPorts[instruction->F].location = computeAddress(currentState, instruction);
    }
    else{
        currentState->nextInstructionAddress--;
    }
    return 1;    return 1;
}
int IN(struct mixState * currentState, struct mixInstruction * instruction){
    if (currentState->ioPorts[instruction->F].busy == 0){
        currentState->ioPorts[instruction->F].in = 1;
        currentState->ioPorts[instruction->F].out = 0;
        currentState->ioPorts[instruction->F].ioc = 0;
        currentState->ioPorts[instruction->F].location = computeAddress(currentState, instruction);
    }
    else{
        currentState->nextInstructionAddress--;
    }
    return 1;
}
int OUT(struct mixState * currentState, struct mixInstruction * instruction){
    if (currentState->ioPorts[instruction->F].busy == 0){
        currentState->ioPorts[instruction->F].in = 0;
        currentState->ioPorts[instruction->F].out = 1;
        currentState->ioPorts[instruction->F].ioc = 0;
        currentState->ioPorts[instruction->F].location = computeAddress(currentState, instruction);
    }
    else{
        currentState->nextInstructionAddress--;
    }
    return 1;    return 1;
}
int JRED(struct mixState * currentState, struct mixInstruction * instruction){
    if(currentState->ioPorts[instruction->F].busy == 0) {
        int memoryAddress;
        int J1, J2;
        memoryAddress = computeAddress(currentState, instruction);
        J1 = currentState->nextInstructionAddress / currentState->mixByteStates;
        J2 = currentState->nextInstructionAddress % currentState->mixByteStates;
        currentState->rJ[0] = J1;
        currentState->rJ[1] = J2;
        currentState->nextInstructionAddress = memoryAddress;
    }
    return 1;
}