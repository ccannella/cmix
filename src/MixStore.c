#include "MixStore.h"

int STA(struct mixState * currentState, struct mixInstruction * instruction){
    copyRegister(currentState, currentState->rA, instruction);
    return 2;
}

int STX(struct mixState * currentState, struct mixInstruction * instruction){
    copyRegister(currentState, currentState->rX, instruction);
    return 2;
}

int ST1(struct mixState * currentState, struct mixInstruction * instruction){
    int temp[6] = {currentState->rI[0][0],0,0,0,currentState->rI[0][1],currentState->rI[0][2]};
    copyRegister(currentState, temp, instruction);
    return 2;
}

int ST2(struct mixState * currentState, struct mixInstruction * instruction){
    int temp[6] = {currentState->rI[1][0],0,0,0,currentState->rI[1][1],currentState->rI[1][2]};
    copyRegister(currentState, temp, instruction);
    return 2;
}

int ST3(struct mixState * currentState, struct mixInstruction * instruction){
    int temp[6] = {currentState->rI[2][0],0,0,0,currentState->rI[2][1],currentState->rI[2][2]};
    copyRegister(currentState, temp, instruction);
    return 2;
}

int ST4(struct mixState * currentState, struct mixInstruction * instruction){
    int temp[6] = {currentState->rI[3][0],0,0,0,currentState->rI[3][1],currentState->rI[3][2]};
    copyRegister(currentState, temp, instruction);
    return 2;
}

int ST5(struct mixState * currentState, struct mixInstruction * instruction){
    int temp[6] = {currentState->rI[4][0],0,0,0,currentState->rI[4][1],currentState->rI[4][2]};
    copyRegister(currentState, temp, instruction);
    return 2;
}

int ST6(struct mixState * currentState, struct mixInstruction * instruction){
    int temp[6] = {currentState->rI[5][0],0,0,0,currentState->rI[5][1],currentState->rI[5][2]};
    copyRegister(currentState, temp, instruction);
    return 2;
}

int STJ(struct mixState * currentState, struct mixInstruction * instruction){
    int temp[6] = {0,0,0,0,currentState->rJ[0],currentState->rJ[1]};
    copyRegister(currentState, temp, instruction);
    return 2;
}

int STZ(struct mixState * currentState, struct mixInstruction * instruction){
    int temp[6] = {0,0,0,0,0,0};
    copyRegister(currentState, temp, instruction);
    return 2;
}

