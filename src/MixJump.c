#include "MixJump.h"

int JMP(struct mixState * currentState, struct mixInstruction * instruction){
    int memoryAddress;
    int J1, J2;
    memoryAddress = computeAddress(currentState, instruction);
    J1 = currentState->nextInstructionAddress/currentState->mixByteStates;
    J2 = currentState->nextInstructionAddress%currentState->mixByteStates;
    currentState->rJ[0] = J1;
    currentState->rJ[1] = J2;
    currentState->nextInstructionAddress = memoryAddress;
    return 1;
}

int JSJ(struct mixState * currentState, struct mixInstruction * instruction){
    int memoryAddress;
    memoryAddress = computeAddress(currentState, instruction);
    currentState->nextInstructionAddress = memoryAddress;
    return 1;
}

int JOV(struct mixState * currentState, struct mixInstruction * instruction){
    if(currentState->overflow == 1){
        int memoryAddress;
        int J1, J2;
        memoryAddress = computeAddress(currentState, instruction);
        J1 = currentState->nextInstructionAddress/currentState->mixByteStates;
        J2 = currentState->nextInstructionAddress%currentState->mixByteStates;
        currentState->rJ[0] = J1;
        currentState->rJ[1] = J2;
        currentState->nextInstructionAddress = memoryAddress;
        currentState->overflow = 0;
    }
    return 1;
}

int JNOV(struct mixState * currentState, struct mixInstruction * instruction){
    if(currentState->overflow == 0){
        int memoryAddress;
        int J1, J2;
        memoryAddress = computeAddress(currentState, instruction);
        J1 = currentState->nextInstructionAddress/currentState->mixByteStates;
        J2 = currentState->nextInstructionAddress%currentState->mixByteStates;
        currentState->rJ[0] = J1;
        currentState->rJ[1] = J2;
        currentState->nextInstructionAddress = memoryAddress;
    }
    else{
        currentState->overflow = 1;
    }
    return 1;
}

int JL(struct mixState * currentState, struct mixInstruction * instruction){
    if(currentState->comparison == -1){
        int memoryAddress;
        int J1, J2;
        memoryAddress = computeAddress(currentState, instruction);
        J1 = currentState->nextInstructionAddress/currentState->mixByteStates;
        J2 = currentState->nextInstructionAddress%currentState->mixByteStates;
        currentState->rJ[0] = J1;
        currentState->rJ[1] = J2;
        currentState->nextInstructionAddress = memoryAddress;
    }
    return 1;
}


int JE(struct mixState * currentState, struct mixInstruction * instruction){
    if(currentState->comparison == 0){
        int memoryAddress;
        int J1, J2;
        memoryAddress = computeAddress(currentState, instruction);
        J1 = currentState->nextInstructionAddress/currentState->mixByteStates;
        J2 = currentState->nextInstructionAddress%currentState->mixByteStates;
        currentState->rJ[0] = J1;
        currentState->rJ[1] = J2;
        currentState->nextInstructionAddress = memoryAddress;
    }
    return 1;
}


int JG(struct mixState * currentState, struct mixInstruction * instruction){
    if(currentState->comparison == 1){
        int memoryAddress;
        int J1, J2;
        memoryAddress = computeAddress(currentState, instruction);
        J1 = currentState->nextInstructionAddress/currentState->mixByteStates;
        J2 = currentState->nextInstructionAddress%currentState->mixByteStates;
        currentState->rJ[0] = J1;
        currentState->rJ[1] = J2;
        currentState->nextInstructionAddress = memoryAddress;
    }
    return 1;
}


int JGE(struct mixState * currentState, struct mixInstruction * instruction){
    if(currentState->comparison >= 0){
        int memoryAddress;
        int J1, J2;
        memoryAddress = computeAddress(currentState, instruction);
        J1 = currentState->nextInstructionAddress/currentState->mixByteStates;
        J2 = currentState->nextInstructionAddress%currentState->mixByteStates;
        currentState->rJ[0] = J1;
        currentState->rJ[1] = J2;
        currentState->nextInstructionAddress = memoryAddress;
    }
    return 1;
}


int JNE(struct mixState * currentState, struct mixInstruction * instruction){
    if(currentState->comparison != 0){
        int memoryAddress;
        int J1, J2;
        memoryAddress = computeAddress(currentState, instruction);
        J1 = currentState->nextInstructionAddress/currentState->mixByteStates;
        J2 = currentState->nextInstructionAddress%currentState->mixByteStates;
        currentState->rJ[0] = J1;
        currentState->rJ[1] = J2;
        currentState->nextInstructionAddress = memoryAddress;
    }
    return 1;
}

int JLE(struct mixState * currentState, struct mixInstruction * instruction){
    if(currentState->comparison <= 0){
        int memoryAddress;
        int J1, J2;
        memoryAddress = computeAddress(currentState, instruction);
        J1 = currentState->nextInstructionAddress/currentState->mixByteStates;
        J2 = currentState->nextInstructionAddress%currentState->mixByteStates;
        currentState->rJ[0] = J1;
        currentState->rJ[1] = J2;
        currentState->nextInstructionAddress = memoryAddress;
    }
    return 1;
}

int typicalJumpControl(struct mixState * currentState, struct mixInstruction * instruction){
    if(instruction->F == 0){
        return JMP(currentState, instruction);
    }
    else if(instruction->F == 1){
        return JSJ(currentState, instruction);
    }
    else if(instruction->F == 2){
        return JOV(currentState, instruction);
    }
    else if(instruction->F == 3){
        return JNOV(currentState, instruction);
    }
    else if(instruction->F == 4){
        return JL(currentState, instruction);
    }
    else if(instruction->F == 5){
        return JE(currentState, instruction);
    }
    else if(instruction->F == 6){
        return JG(currentState, instruction);
    }
    else if(instruction->F == 7){
        return JGE(currentState, instruction);
    }
    else if(instruction->F == 8){
        return JNE(currentState, instruction);
    }
    else if(instruction->F == 9){
        return JLE(currentState, instruction);
    }
}

int JAN(struct mixState * currentState, struct mixInstruction * instruction){
    int allZero, i;
    allZero = 0;
    for (i = 1; i < 6 && allZero == 0; i++){
        if(currentState->rA[i] != 0){
            allZero = 1;
        }
    }
    if(currentState->rA[0] == 1 && allZero == 0){
        int memoryAddress;
        int J1, J2;
        memoryAddress = computeAddress(currentState, instruction);
        J1 = currentState->nextInstructionAddress/currentState->mixByteStates;
        J2 = currentState->nextInstructionAddress%currentState->mixByteStates;
        currentState->rJ[0] = J1;
        currentState->rJ[1] = J2;
        currentState->nextInstructionAddress = memoryAddress;
    }    
    return 1;
}

int JAZ(struct mixState * currentState, struct mixInstruction * instruction){
    int allZero, i;
    allZero = 0;
    for (i = 1; i < 6 && allZero == 0; i++){
        if(currentState->rA[i] != 0){
            allZero = 1;
        }
    }
    if(allZero == 1){
        int memoryAddress;
        int J1, J2;
        memoryAddress = computeAddress(currentState, instruction);
        J1 = currentState->nextInstructionAddress/currentState->mixByteStates;
        J2 = currentState->nextInstructionAddress%currentState->mixByteStates;
        currentState->rJ[0] = J1;
        currentState->rJ[1] = J2;
        currentState->nextInstructionAddress = memoryAddress;
    }    
    return 1;
}

int JAP(struct mixState * currentState, struct mixInstruction * instruction){
    int allZero, i;
    allZero = 0;
    for (i = 1; i < 6 && allZero == 0; i++){
        if(currentState->rA[i] != 0){
            allZero = 1;
        }
    }
    if(currentState->rA[0] == 0 && allZero == 0){
        int memoryAddress;
        int J1, J2;
        memoryAddress = computeAddress(currentState, instruction);
        J1 = currentState->nextInstructionAddress/currentState->mixByteStates;
        J2 = currentState->nextInstructionAddress%currentState->mixByteStates;
        currentState->rJ[0] = J1;
        currentState->rJ[1] = J2;
        currentState->nextInstructionAddress = memoryAddress;
    }    
    return 1;
}

int JANN(struct mixState * currentState, struct mixInstruction * instruction){
    int allZero, i;
    allZero = 0;
    for (i = 1; i < 6 && allZero == 0; i++){
        if(currentState->rA[i] != 0){
            allZero = 1;
        }
    }
    if(currentState->rA[0] != 1 || allZero == 1){
        int memoryAddress;
        int J1, J2;
        memoryAddress = computeAddress(currentState, instruction);
        J1 = currentState->nextInstructionAddress/currentState->mixByteStates;
        J2 = currentState->nextInstructionAddress%currentState->mixByteStates;
        currentState->rJ[0] = J1;
        currentState->rJ[1] = J2;
        currentState->nextInstructionAddress = memoryAddress;
    }    
    return 1;
}

int JANZ(struct mixState * currentState, struct mixInstruction * instruction){
    int allZero, i;
    allZero = 0;
    for (i = 1; i < 6 && allZero == 0; i++){
        if(currentState->rA[i] != 0){
            allZero = 1;
        }
    }
    if(allZero == 0){
        int memoryAddress;
        int J1, J2;
        memoryAddress = computeAddress(currentState, instruction);
        J1 = currentState->nextInstructionAddress/currentState->mixByteStates;
        J2 = currentState->nextInstructionAddress%currentState->mixByteStates;
        currentState->rJ[0] = J1;
        currentState->rJ[1] = J2;
        currentState->nextInstructionAddress = memoryAddress;
    }    
    return 1;
}

int JANP(struct mixState * currentState, struct mixInstruction * instruction){
    int allZero, i;
    allZero = 0;
    for (i = 1; i < 6 && allZero == 0; i++){
        if(currentState->rA[i] != 0){
            allZero = 1;
        }
    }
    if(currentState->rA[0] != 0 || allZero == 1){
        int memoryAddress;
        int J1, J2;
        memoryAddress = computeAddress(currentState, instruction);
        J1 = currentState->nextInstructionAddress/currentState->mixByteStates;
        J2 = currentState->nextInstructionAddress%currentState->mixByteStates;
        currentState->rJ[0] = J1;
        currentState->rJ[1] = J2;
        currentState->nextInstructionAddress = memoryAddress;
    }    
    return 1;
}

int registerJumpControlA(struct mixState * currentState, struct mixInstruction * instruction){
    if(instruction->F == 0){
        return JAN(currentState, instruction);
    }
    else if(instruction->F == 1){
        return JAZ(currentState, instruction);
    }
    else if(instruction->F == 2){
        return JAP(currentState, instruction);
    }
    else if(instruction->F == 3){
        return JANN(currentState, instruction);
    }
    else if(instruction->F == 4){
        return JANZ(currentState, instruction);
    }
    else if(instruction->F == 5){
        return JANP(currentState, instruction);
    }
}

int JXN(struct mixState * currentState, struct mixInstruction * instruction){
    int allZero, i;
    allZero = 0;
    for (i = 1; i < 6 && allZero == 0; i++){
        if(currentState->rX[i] != 0){
            allZero = 1;
        }
    }
    if(currentState->rX[0] == 1 && allZero == 0){
        int memoryAddress;
        int J1, J2;
        memoryAddress = computeAddress(currentState, instruction);
        J1 = currentState->nextInstructionAddress/currentState->mixByteStates;
        J2 = currentState->nextInstructionAddress%currentState->mixByteStates;
        currentState->rJ[0] = J1;
        currentState->rJ[1] = J2;
        currentState->nextInstructionAddress = memoryAddress;
    }    
    return 1;
}

int JXZ(struct mixState * currentState, struct mixInstruction * instruction){
    int allZero, i;
    allZero = 0;
    for (i = 1; i < 6 && allZero == 0; i++){
        if(currentState->rX[i] != 0){
            allZero = 1;
        }
    }
    if(allZero == 1){
        int memoryAddress;
        int J1, J2;
        memoryAddress = computeAddress(currentState, instruction);
        J1 = currentState->nextInstructionAddress/currentState->mixByteStates;
        J2 = currentState->nextInstructionAddress%currentState->mixByteStates;
        currentState->rJ[0] = J1;
        currentState->rJ[1] = J2;
        currentState->nextInstructionAddress = memoryAddress;
    }    
    return 1;
}

int JXP(struct mixState * currentState, struct mixInstruction * instruction){
    int allZero, i;
    allZero = 0;
    for (i = 1; i < 6 && allZero == 0; i++){
        if(currentState->rX[i] != 0){
            allZero = 1;
        }
    }
    if(currentState->rX[0] == 0 && allZero == 0){
        int memoryAddress;
        int J1, J2;
        memoryAddress = computeAddress(currentState, instruction);
        J1 = currentState->nextInstructionAddress/currentState->mixByteStates;
        J2 = currentState->nextInstructionAddress%currentState->mixByteStates;
        currentState->rJ[0] = J1;
        currentState->rJ[1] = J2;
        currentState->nextInstructionAddress = memoryAddress;
    }    
    return 1;
}

int JXNN(struct mixState * currentState, struct mixInstruction * instruction){
    int allZero, i;
    allZero = 0;
    for (i = 1; i < 6 && allZero == 0; i++){
        if(currentState->rX[i] != 0){
            allZero = 1;
        }
    }
    if(currentState->rX[0] != 1 || allZero == 1){
        int memoryAddress;
        int J1, J2;
        memoryAddress = computeAddress(currentState, instruction);
        J1 = currentState->nextInstructionAddress/currentState->mixByteStates;
        J2 = currentState->nextInstructionAddress%currentState->mixByteStates;
        currentState->rJ[0] = J1;
        currentState->rJ[1] = J2;
        currentState->nextInstructionAddress = memoryAddress;
    }    
    return 1;
}

int JXNZ(struct mixState * currentState, struct mixInstruction * instruction){
    int allZero, i;
    allZero = 0;
    for (i = 1; i < 6 && allZero == 0; i++){
        if(currentState->rX[i] != 0){
            allZero = 1;
        }
    }
    if(allZero == 0){
        int memoryAddress;
        int J1, J2;
        memoryAddress = computeAddress(currentState, instruction);
        J1 = currentState->nextInstructionAddress/currentState->mixByteStates;
        J2 = currentState->nextInstructionAddress%currentState->mixByteStates;
        currentState->rJ[0] = J1;
        currentState->rJ[1] = J2;
        currentState->nextInstructionAddress = memoryAddress;
    }    
    return 1;
}

int JXNP(struct mixState * currentState, struct mixInstruction * instruction){
    int allZero, i;
    allZero = 0;
    for (i = 1; i < 6 && allZero == 0; i++){
        if(currentState->rX[i] != 0){
            allZero = 1;
        }
    }
    if(currentState->rX[0] != 0 || allZero == 1){
        int memoryAddress;
        int J1, J2;
        memoryAddress = computeAddress(currentState, instruction);
        J1 = currentState->nextInstructionAddress/currentState->mixByteStates;
        J2 = currentState->nextInstructionAddress%currentState->mixByteStates;
        currentState->rJ[0] = J1;
        currentState->rJ[1] = J2;
        currentState->nextInstructionAddress = memoryAddress;
    }    
    return 1;
}

int registerJumpControlX(struct mixState * currentState, struct mixInstruction * instruction){
    if(instruction->F == 0){
        return JXN(currentState, instruction);
    }
    else if(instruction->F == 1){
        return JXZ(currentState, instruction);
    }
    else if(instruction->F == 2){
        return JXP(currentState, instruction);
    }
    else if(instruction->F == 3){
        return JXNN(currentState, instruction);
    }
    else if(instruction->F == 4){
        return JXNZ(currentState, instruction);
    }
    else if(instruction->F == 5){
        return JXNP(currentState, instruction);
    }
}

int J1N(struct mixState * currentState, struct mixInstruction * instruction){
    int allZero, i;
    allZero = 0;
    for (i = 1; i < 3 && allZero == 0; i++){
        if(currentState->rI[0][i] != 0){
            allZero = 1;
        }
    }
    if(currentState->rI[0][0] == 1 && allZero == 0){
        int memoryAddress;
        int J1, J2;
        memoryAddress = computeAddress(currentState, instruction);
        J1 = currentState->nextInstructionAddress/currentState->mixByteStates;
        J2 = currentState->nextInstructionAddress%currentState->mixByteStates;
        currentState->rJ[0] = J1;
        currentState->rJ[1] = J2;
        currentState->nextInstructionAddress = memoryAddress;
    }    
    return 1;
}

int J1Z(struct mixState * currentState, struct mixInstruction * instruction){
    int allZero, i;
    allZero = 0;
    for (i = 1; i < 3 && allZero == 0; i++){
        if(currentState->rI[0][i] != 0){
            allZero = 1;
        }
    }
    if(allZero == 1){
        int memoryAddress;
        int J1, J2;
        memoryAddress = computeAddress(currentState, instruction);
        J1 = currentState->nextInstructionAddress/currentState->mixByteStates;
        J2 = currentState->nextInstructionAddress%currentState->mixByteStates;
        currentState->rJ[0] = J1;
        currentState->rJ[1] = J2;
        currentState->nextInstructionAddress = memoryAddress;
    }    
    return 1;
}

int J1P(struct mixState * currentState, struct mixInstruction * instruction){
    int allZero, i;
    allZero = 0;
    for (i = 1; i < 3 && allZero == 0; i++){
        if(currentState->rI[0][i] != 0){
            allZero = 1;
        }
    }
    if(currentState->rI[0][0] == 0 && allZero == 0){
        int memoryAddress;
        int J1, J2;
        memoryAddress = computeAddress(currentState, instruction);
        J1 = currentState->nextInstructionAddress/currentState->mixByteStates;
        J2 = currentState->nextInstructionAddress%currentState->mixByteStates;
        currentState->rJ[0] = J1;
        currentState->rJ[1] = J2;
        currentState->nextInstructionAddress = memoryAddress;
    }    
    return 1;
}

int J1NN(struct mixState * currentState, struct mixInstruction * instruction){
    int allZero, i;
    allZero = 0;
    for (i = 1; i < 3 && allZero == 0; i++){
        if(currentState->rI[0][i] != 0){
            allZero = 1;
        }
    }
    if(currentState->rI[0][0] != 1 || allZero == 1){
        int memoryAddress;
        int J1, J2;
        memoryAddress = computeAddress(currentState, instruction);
        J1 = currentState->nextInstructionAddress/currentState->mixByteStates;
        J2 = currentState->nextInstructionAddress%currentState->mixByteStates;
        currentState->rJ[0] = J1;
        currentState->rJ[1] = J2;
        currentState->nextInstructionAddress = memoryAddress;
    }    
    return 1;
}

int J1NZ(struct mixState * currentState, struct mixInstruction * instruction){
    int allZero, i;
    allZero = 0;
    for (i = 1; i < 3 && allZero == 0; i++){
        if(currentState->rI[0][i] != 0){
            allZero = 1;
        }
    }
    if(allZero == 0){
        int memoryAddress;
        int J1, J2;
        memoryAddress = computeAddress(currentState, instruction);
        J1 = currentState->nextInstructionAddress/currentState->mixByteStates;
        J2 = currentState->nextInstructionAddress%currentState->mixByteStates;
        currentState->rJ[0] = J1;
        currentState->rJ[1] = J2;
        currentState->nextInstructionAddress = memoryAddress;
    }    
    return 1;
}

int J1NP(struct mixState * currentState, struct mixInstruction * instruction){
    int allZero, i;
    allZero = 0;
    for (i = 1; i < 3 && allZero == 0; i++){
        if(currentState->rI[0][i] != 0){
            allZero = 1;
        }
    }
    if(currentState->rI[0][0] != 0 || allZero == 1){
        int memoryAddress;
        int J1, J2;
        memoryAddress = computeAddress(currentState, instruction);
        J1 = currentState->nextInstructionAddress/currentState->mixByteStates;
        J2 = currentState->nextInstructionAddress%currentState->mixByteStates;
        currentState->rJ[0] = J1;
        currentState->rJ[1] = J2;
        currentState->nextInstructionAddress = memoryAddress;
    }    
    return 1;
}

int registerJumpControl1(struct mixState * currentState, struct mixInstruction * instruction){
    if(instruction->F == 0){
        return J1N(currentState, instruction);
    }
    else if(instruction->F == 1){
        return J1Z(currentState, instruction);
    }
    else if(instruction->F == 2){
        return J1P(currentState, instruction);
    }
    else if(instruction->F == 3){
        return J1NN(currentState, instruction);
    }
    else if(instruction->F == 4){
        return J1NZ(currentState, instruction);
    }
    else if(instruction->F == 5){
        return J1NP(currentState, instruction);
    }
}

int J2N(struct mixState * currentState, struct mixInstruction * instruction){
    int allZero, i;
    allZero = 0;
    for (i = 1; i < 3 && allZero == 0; i++){
        if(currentState->rI[1][i] != 0){
            allZero = 1;
        }
    }
    if(currentState->rI[1][0] == 1 && allZero == 0){
        int memoryAddress;
        int J1, J2;
        memoryAddress = computeAddress(currentState, instruction);
        J1 = currentState->nextInstructionAddress/currentState->mixByteStates;
        J2 = currentState->nextInstructionAddress%currentState->mixByteStates;
        currentState->rJ[0] = J1;
        currentState->rJ[1] = J2;
        currentState->nextInstructionAddress = memoryAddress;
    }    
    return 1;
}

int J2Z(struct mixState * currentState, struct mixInstruction * instruction){
    int allZero, i;
    allZero = 0;
    for (i = 1; i < 3 && allZero == 0; i++){
        if(currentState->rI[1][i] != 0){
            allZero = 1;
        }
    }
    if(allZero == 1){
        int memoryAddress;
        int J1, J2;
        memoryAddress = computeAddress(currentState, instruction);
        J1 = currentState->nextInstructionAddress/currentState->mixByteStates;
        J2 = currentState->nextInstructionAddress%currentState->mixByteStates;
        currentState->rJ[0] = J1;
        currentState->rJ[1] = J2;
        currentState->nextInstructionAddress = memoryAddress;
    }    
    return 1;
}

int J2P(struct mixState * currentState, struct mixInstruction * instruction){
    int allZero, i;
    allZero = 0;
    for (i = 1; i < 3 && allZero == 0; i++){
        if(currentState->rI[1][i] != 0){
            allZero = 1;
        }
    }
    if(currentState->rI[1][0] == 0 && allZero == 0){
        int memoryAddress;
        int J1, J2;
        memoryAddress = computeAddress(currentState, instruction);
        J1 = currentState->nextInstructionAddress/currentState->mixByteStates;
        J2 = currentState->nextInstructionAddress%currentState->mixByteStates;
        currentState->rJ[0] = J1;
        currentState->rJ[1] = J2;
        currentState->nextInstructionAddress = memoryAddress;
    }    
    return 1;
}

int J2NN(struct mixState * currentState, struct mixInstruction * instruction){
    int allZero, i;
    allZero = 0;
    for (i = 1; i < 3 && allZero == 0; i++){
        if(currentState->rI[1][i] != 0){
            allZero = 1;
        }
    }
    if(currentState->rI[1][0] != 1 || allZero == 1){
        int memoryAddress;
        int J1, J2;
        memoryAddress = computeAddress(currentState, instruction);
        J1 = currentState->nextInstructionAddress/currentState->mixByteStates;
        J2 = currentState->nextInstructionAddress%currentState->mixByteStates;
        currentState->rJ[0] = J1;
        currentState->rJ[1] = J2;
        currentState->nextInstructionAddress = memoryAddress;
    }    
    return 1;
}

int J2NZ(struct mixState * currentState, struct mixInstruction * instruction){
    int allZero, i;
    allZero = 0;
    for (i = 1; i < 3 && allZero == 0; i++){
        if(currentState->rI[1][i] != 0){
            allZero = 1;
        }
    }
    if(allZero == 0){
        int memoryAddress;
        int J1, J2;
        memoryAddress = computeAddress(currentState, instruction);
        J1 = currentState->nextInstructionAddress/currentState->mixByteStates;
        J2 = currentState->nextInstructionAddress%currentState->mixByteStates;
        currentState->rJ[0] = J1;
        currentState->rJ[1] = J2;
        currentState->nextInstructionAddress = memoryAddress;
    }    
    return 1;
}

int J2NP(struct mixState * currentState, struct mixInstruction * instruction){
    int allZero, i;
    allZero = 0;
    for (i = 1; i < 3 && allZero == 0; i++){
        if(currentState->rI[1][i] != 0){
            allZero = 1;
        }
    }
    if(currentState->rI[1][0] != 0 || allZero == 1){
        int memoryAddress;
        int J1, J2;
        memoryAddress = computeAddress(currentState, instruction);
        J1 = currentState->nextInstructionAddress/currentState->mixByteStates;
        J2 = currentState->nextInstructionAddress%currentState->mixByteStates;
        currentState->rJ[0] = J1;
        currentState->rJ[1] = J2;
        currentState->nextInstructionAddress = memoryAddress;
    }    
    return 1;
}

int registerJumpControl2(struct mixState * currentState, struct mixInstruction * instruction){
    if(instruction->F == 0){
        return J2N(currentState, instruction);
    }
    else if(instruction->F == 1){
        return J2Z(currentState, instruction);
    }
    else if(instruction->F == 2){
        return J2P(currentState, instruction);
    }
    else if(instruction->F == 3){
        return J2NN(currentState, instruction);
    }
    else if(instruction->F == 4){
        return J2NZ(currentState, instruction);
    }
    else if(instruction->F == 5){
        return J2NP(currentState, instruction);
    }
}

int J3N(struct mixState * currentState, struct mixInstruction * instruction){
    int allZero, i;
    allZero = 0;
    for (i = 1; i < 3 && allZero == 0; i++){
        if(currentState->rI[2][i] != 0){
            allZero = 1;
        }
    }
    if(currentState->rI[2][0] == 1 && allZero == 0){
        int memoryAddress;
        int J1, J2;
        memoryAddress = computeAddress(currentState, instruction);
        J1 = currentState->nextInstructionAddress/currentState->mixByteStates;
        J2 = currentState->nextInstructionAddress%currentState->mixByteStates;
        currentState->rJ[0] = J1;
        currentState->rJ[1] = J2;
        currentState->nextInstructionAddress = memoryAddress;
    }    
    return 1;
}

int J3Z(struct mixState * currentState, struct mixInstruction * instruction){
    int allZero, i;
    allZero = 0;
    for (i = 1; i < 3 && allZero == 0; i++){
        if(currentState->rI[2][i] != 0){
            allZero = 1;
        }
    }
    if(allZero == 1){
        int memoryAddress;
        int J1, J2;
        memoryAddress = computeAddress(currentState, instruction);
        J1 = currentState->nextInstructionAddress/currentState->mixByteStates;
        J2 = currentState->nextInstructionAddress%currentState->mixByteStates;
        currentState->rJ[0] = J1;
        currentState->rJ[1] = J2;
        currentState->nextInstructionAddress = memoryAddress;
    }    
    return 1;
}

int J3P(struct mixState * currentState, struct mixInstruction * instruction){
    int allZero, i;
    allZero = 0;
    for (i = 1; i < 3 && allZero == 0; i++){
        if(currentState->rI[2][i] != 0){
            allZero = 1;
        }
    }
    if(currentState->rI[2][0] == 0 && allZero == 0){
        int memoryAddress;
        int J1, J2;
        memoryAddress = computeAddress(currentState, instruction);
        J1 = currentState->nextInstructionAddress/currentState->mixByteStates;
        J2 = currentState->nextInstructionAddress%currentState->mixByteStates;
        currentState->rJ[0] = J1;
        currentState->rJ[1] = J2;
        currentState->nextInstructionAddress = memoryAddress;
    }    
    return 1;
}

int J3NN(struct mixState * currentState, struct mixInstruction * instruction){
    int allZero, i;
    allZero = 0;
    for (i = 1; i < 3 && allZero == 0; i++){
        if(currentState->rI[2][i] != 0){
            allZero = 1;
        }
    }
    if(currentState->rI[2][0] != 1 || allZero == 1){
        int memoryAddress;
        int J1, J2;
        memoryAddress = computeAddress(currentState, instruction);
        J1 = currentState->nextInstructionAddress/currentState->mixByteStates;
        J2 = currentState->nextInstructionAddress%currentState->mixByteStates;
        currentState->rJ[0] = J1;
        currentState->rJ[1] = J2;
        currentState->nextInstructionAddress = memoryAddress;
    }    
    return 1;
}

int J3NZ(struct mixState * currentState, struct mixInstruction * instruction){
    int allZero, i;
    allZero = 0;
    for (i = 1; i < 3 && allZero == 0; i++){
        if(currentState->rI[2][i] != 0){
            allZero = 1;
        }
    }
    if(allZero == 0){
        int memoryAddress;
        int J1, J2;
        memoryAddress = computeAddress(currentState, instruction);
        J1 = currentState->nextInstructionAddress/currentState->mixByteStates;
        J2 = currentState->nextInstructionAddress%currentState->mixByteStates;
        currentState->rJ[0] = J1;
        currentState->rJ[1] = J2;
        currentState->nextInstructionAddress = memoryAddress;
    }    
    return 1;
}

int J3NP(struct mixState * currentState, struct mixInstruction * instruction){
    int allZero, i;
    allZero = 0;
    for (i = 1; i < 3 && allZero == 0; i++){
        if(currentState->rI[2][i] != 0){
            allZero = 1;
        }
    }
    if(currentState->rI[2][0] != 0 || allZero == 1){
        int memoryAddress;
        int J1, J2;
        memoryAddress = computeAddress(currentState, instruction);
        J1 = currentState->nextInstructionAddress/currentState->mixByteStates;
        J2 = currentState->nextInstructionAddress%currentState->mixByteStates;
        currentState->rJ[0] = J1;
        currentState->rJ[1] = J2;
        currentState->nextInstructionAddress = memoryAddress;
    }    
    return 1;
}

int registerJumpControl3(struct mixState * currentState, struct mixInstruction * instruction){
    if(instruction->F == 0){
        return J3N(currentState, instruction);
    }
    else if(instruction->F == 1){
        return J3Z(currentState, instruction);
    }
    else if(instruction->F == 2){
        return J3P(currentState, instruction);
    }
    else if(instruction->F == 3){
        return J3NN(currentState, instruction);
    }
    else if(instruction->F == 4){
        return J3NZ(currentState, instruction);
    }
    else if(instruction->F == 5){
        return J3NP(currentState, instruction);
    }
}

int J4N(struct mixState * currentState, struct mixInstruction * instruction){
    int allZero, i;
    allZero = 0;
    for (i = 1; i < 3 && allZero == 0; i++){
        if(currentState->rI[3][i] != 0){
            allZero = 1;
        }
    }
    if(currentState->rI[3][0] == 1 && allZero == 0){
        int memoryAddress;
        int J1, J2;
        memoryAddress = computeAddress(currentState, instruction);
        J1 = currentState->nextInstructionAddress/currentState->mixByteStates;
        J2 = currentState->nextInstructionAddress%currentState->mixByteStates;
        currentState->rJ[0] = J1;
        currentState->rJ[1] = J2;
        currentState->nextInstructionAddress = memoryAddress;
    }    
    return 1;
}

int J4Z(struct mixState * currentState, struct mixInstruction * instruction){
    int allZero, i;
    allZero = 0;
    for (i = 1; i < 3 && allZero == 0; i++){
        if(currentState->rI[3][i] != 0){
            allZero = 1;
        }
    }
    if(allZero == 1){
        int memoryAddress;
        int J1, J2;
        memoryAddress = computeAddress(currentState, instruction);
        J1 = currentState->nextInstructionAddress/currentState->mixByteStates;
        J2 = currentState->nextInstructionAddress%currentState->mixByteStates;
        currentState->rJ[0] = J1;
        currentState->rJ[1] = J2;
        currentState->nextInstructionAddress = memoryAddress;
    }    
    return 1;
}

int J4P(struct mixState * currentState, struct mixInstruction * instruction){
    int allZero, i;
    allZero = 0;
    for (i = 1; i < 3 && allZero == 0; i++){
        if(currentState->rI[3][i] != 0){
            allZero = 1;
        }
    }
    if(currentState->rI[3][0] == 0 && allZero == 0){
        int memoryAddress;
        int J1, J2;
        memoryAddress = computeAddress(currentState, instruction);
        J1 = currentState->nextInstructionAddress/currentState->mixByteStates;
        J2 = currentState->nextInstructionAddress%currentState->mixByteStates;
        currentState->rJ[0] = J1;
        currentState->rJ[1] = J2;
        currentState->nextInstructionAddress = memoryAddress;
    }    
    return 1;
}

int J4NN(struct mixState * currentState, struct mixInstruction * instruction){
    int allZero, i;
    allZero = 0;
    for (i = 1; i < 3 && allZero == 0; i++){
        if(currentState->rI[3][i] != 0){
            allZero = 1;
        }
    }
    if(currentState->rI[3][0] != 1 || allZero == 1){
        int memoryAddress;
        int J1, J2;
        memoryAddress = computeAddress(currentState, instruction);
        J1 = currentState->nextInstructionAddress/currentState->mixByteStates;
        J2 = currentState->nextInstructionAddress%currentState->mixByteStates;
        currentState->rJ[0] = J1;
        currentState->rJ[1] = J2;
        currentState->nextInstructionAddress = memoryAddress;
    }    
    return 1;
}

int J4NZ(struct mixState * currentState, struct mixInstruction * instruction){
    int allZero, i;
    allZero = 0;
    for (i = 1; i < 3 && allZero == 0; i++){
        if(currentState->rI[3][i] != 0){
            allZero = 1;
        }
    }
    if(allZero == 0){
        int memoryAddress;
        int J1, J2;
        memoryAddress = computeAddress(currentState, instruction);
        J1 = currentState->nextInstructionAddress/currentState->mixByteStates;
        J2 = currentState->nextInstructionAddress%currentState->mixByteStates;
        currentState->rJ[0] = J1;
        currentState->rJ[1] = J2;
        currentState->nextInstructionAddress = memoryAddress;
    }    
    return 1;
}

int J4NP(struct mixState * currentState, struct mixInstruction * instruction){
    int allZero, i;
    allZero = 0;
    for (i = 1; i < 3 && allZero == 0; i++){
        if(currentState->rI[3][i] != 0){
            allZero = 1;
        }
    }
    if(currentState->rI[3][0] != 0 || allZero == 1){
        int memoryAddress;
        int J1, J2;
        memoryAddress = computeAddress(currentState, instruction);
        J1 = currentState->nextInstructionAddress/currentState->mixByteStates;
        J2 = currentState->nextInstructionAddress%currentState->mixByteStates;
        currentState->rJ[0] = J1;
        currentState->rJ[1] = J2;
        currentState->nextInstructionAddress = memoryAddress;
    }    
    return 1;
}

int registerJumpControl4(struct mixState * currentState, struct mixInstruction * instruction){
    if(instruction->F == 0){
        return J4N(currentState, instruction);
    }
    else if(instruction->F == 1){
        return J4Z(currentState, instruction);
    }
    else if(instruction->F == 2){
        return J4P(currentState, instruction);
    }
    else if(instruction->F == 3){
        return J4NN(currentState, instruction);
    }
    else if(instruction->F == 4){
        return J4NZ(currentState, instruction);
    }
    else if(instruction->F == 5){
        return J4NP(currentState, instruction);
    }
}

int J5N(struct mixState * currentState, struct mixInstruction * instruction){
    int allZero, i;
    allZero = 0;
    for (i = 1; i < 3 && allZero == 0; i++){
        if(currentState->rI[4][i] != 0){
            allZero = 1;
        }
    }
    if(currentState->rI[4][0] == 1 && allZero == 0){
        int memoryAddress;
        int J1, J2;
        memoryAddress = computeAddress(currentState, instruction);
        J1 = currentState->nextInstructionAddress/currentState->mixByteStates;
        J2 = currentState->nextInstructionAddress%currentState->mixByteStates;
        currentState->rJ[0] = J1;
        currentState->rJ[1] = J2;
        currentState->nextInstructionAddress = memoryAddress;
    }    
    return 1;
}

int J5Z(struct mixState * currentState, struct mixInstruction * instruction){
    int allZero, i;
    allZero = 0;
    for (i = 1; i < 3 && allZero == 0; i++){
        if(currentState->rI[4][i] != 0){
            allZero = 1;
        }
    }
    if(allZero == 1){
        int memoryAddress;
        int J1, J2;
        memoryAddress = computeAddress(currentState, instruction);
        J1 = currentState->nextInstructionAddress/currentState->mixByteStates;
        J2 = currentState->nextInstructionAddress%currentState->mixByteStates;
        currentState->rJ[0] = J1;
        currentState->rJ[1] = J2;
        currentState->nextInstructionAddress = memoryAddress;
    }    
    return 1;
}

int J5P(struct mixState * currentState, struct mixInstruction * instruction){
    int allZero, i;
    allZero = 0;
    for (i = 1; i < 3 && allZero == 0; i++){
        if(currentState->rI[4][i] != 0){
            allZero = 1;
        }
    }
    if(currentState->rI[4][0] == 0 && allZero == 0){
        int memoryAddress;
        int J1, J2;
        memoryAddress = computeAddress(currentState, instruction);
        J1 = currentState->nextInstructionAddress/currentState->mixByteStates;
        J2 = currentState->nextInstructionAddress%currentState->mixByteStates;
        currentState->rJ[0] = J1;
        currentState->rJ[1] = J2;
        currentState->nextInstructionAddress = memoryAddress;
    }    
    return 1;
}

int J5NN(struct mixState * currentState, struct mixInstruction * instruction){
    int allZero, i;
    allZero = 0;
    for (i = 1; i < 3 && allZero == 0; i++){
        if(currentState->rI[4][i] != 0){
            allZero = 1;
        }
    }
    if(currentState->rI[4][0] != 1 || allZero == 1){
        int memoryAddress;
        int J1, J2;
        memoryAddress = computeAddress(currentState, instruction);
        J1 = currentState->nextInstructionAddress/currentState->mixByteStates;
        J2 = currentState->nextInstructionAddress%currentState->mixByteStates;
        currentState->rJ[0] = J1;
        currentState->rJ[1] = J2;
        currentState->nextInstructionAddress = memoryAddress;
    }    
    return 1;
}

int J5NZ(struct mixState * currentState, struct mixInstruction * instruction){
    int allZero, i;
    allZero = 0;
    for (i = 1; i < 3 && allZero == 0; i++){
        if(currentState->rI[4][i] != 0){
            allZero = 1;
        }
    }
    if(allZero == 0){
        int memoryAddress;
        int J1, J2;
        memoryAddress = computeAddress(currentState, instruction);
        J1 = currentState->nextInstructionAddress/currentState->mixByteStates;
        J2 = currentState->nextInstructionAddress%currentState->mixByteStates;
        currentState->rJ[0] = J1;
        currentState->rJ[1] = J2;
        currentState->nextInstructionAddress = memoryAddress;
    }    
    return 1;
}

int J5NP(struct mixState * currentState, struct mixInstruction * instruction){
    int allZero, i;
    allZero = 0;
    for (i = 1; i < 3 && allZero == 0; i++){
        if(currentState->rI[4][i] != 0){
            allZero = 1;
        }
    }
    if(currentState->rI[4][0] != 0 || allZero == 1){
        int memoryAddress;
        int J1, J2;
        memoryAddress = computeAddress(currentState, instruction);
        J1 = currentState->nextInstructionAddress/currentState->mixByteStates;
        J2 = currentState->nextInstructionAddress%currentState->mixByteStates;
        currentState->rJ[0] = J1;
        currentState->rJ[1] = J2;
        currentState->nextInstructionAddress = memoryAddress;
    }    
    return 1;
}

int registerJumpControl5(struct mixState * currentState, struct mixInstruction * instruction){
    if(instruction->F == 0){
        return J5N(currentState, instruction);
    }
    else if(instruction->F == 1){
        return J5Z(currentState, instruction);
    }
    else if(instruction->F == 2){
        return J5P(currentState, instruction);
    }
    else if(instruction->F == 3){
        return J5NN(currentState, instruction);
    }
    else if(instruction->F == 4){
        return J5NZ(currentState, instruction);
    }
    else if(instruction->F == 5){
        return J5NP(currentState, instruction);
    }
}

int J6N(struct mixState * currentState, struct mixInstruction * instruction){
    int allZero, i;
    allZero = 0;
    for (i = 1; i < 3 && allZero == 0; i++){
        if(currentState->rI[5][i] != 0){
            allZero = 1;
        }
    }
    if(currentState->rI[5][0] == 1 && allZero == 0){
        int memoryAddress;
        int J1, J2;
        memoryAddress = computeAddress(currentState, instruction);
        J1 = currentState->nextInstructionAddress/currentState->mixByteStates;
        J2 = currentState->nextInstructionAddress%currentState->mixByteStates;
        currentState->rJ[0] = J1;
        currentState->rJ[1] = J2;
        currentState->nextInstructionAddress = memoryAddress;
    }    
    return 1;
}

int J6Z(struct mixState * currentState, struct mixInstruction * instruction){
    int allZero, i;
    allZero = 0;
    for (i = 1; i < 3 && allZero == 0; i++){
        if(currentState->rI[5][i] != 0){
            allZero = 1;
        }
    }
    if(allZero == 1){
        int memoryAddress;
        int J1, J2;
        memoryAddress = computeAddress(currentState, instruction);
        J1 = currentState->nextInstructionAddress/currentState->mixByteStates;
        J2 = currentState->nextInstructionAddress%currentState->mixByteStates;
        currentState->rJ[0] = J1;
        currentState->rJ[1] = J2;
        currentState->nextInstructionAddress = memoryAddress;
    }    
    return 1;
}

int J6P(struct mixState * currentState, struct mixInstruction * instruction){
    int allZero, i;
    allZero = 0;
    for (i = 1; i < 3 && allZero == 0; i++){
        if(currentState->rI[5][i] != 0){
            allZero = 1;
        }
    }
    if(currentState->rI[5][0] == 0 && allZero == 0){
        int memoryAddress;
        int J1, J2;
        memoryAddress = computeAddress(currentState, instruction);
        J1 = currentState->nextInstructionAddress/currentState->mixByteStates;
        J2 = currentState->nextInstructionAddress%currentState->mixByteStates;
        currentState->rJ[0] = J1;
        currentState->rJ[1] = J2;
        currentState->nextInstructionAddress = memoryAddress;
    }    
    return 1;
}

int J6NN(struct mixState * currentState, struct mixInstruction * instruction){
    int allZero, i;
    allZero = 0;
    for (i = 1; i < 3 && allZero == 0; i++){
        if(currentState->rI[5][i] != 0){
            allZero = 1;
        }
    }
    if(currentState->rI[5][0] != 1 || allZero == 1){
        int memoryAddress;
        int J1, J2;
        memoryAddress = computeAddress(currentState, instruction);
        J1 = currentState->nextInstructionAddress/currentState->mixByteStates;
        J2 = currentState->nextInstructionAddress%currentState->mixByteStates;
        currentState->rJ[0] = J1;
        currentState->rJ[1] = J2;
        currentState->nextInstructionAddress = memoryAddress;
    }    
    return 1;
}

int J6NZ(struct mixState * currentState, struct mixInstruction * instruction){
    int allZero, i;
    allZero = 0;
    for (i = 1; i < 3 && allZero == 0; i++){
        if(currentState->rI[5][i] != 0){
            allZero = 1;
        }
    }
    if(allZero == 0){
        int memoryAddress;
        int J1, J2;
        memoryAddress = computeAddress(currentState, instruction);
        J1 = currentState->nextInstructionAddress/currentState->mixByteStates;
        J2 = currentState->nextInstructionAddress%currentState->mixByteStates;
        currentState->rJ[0] = J1;
        currentState->rJ[1] = J2;
        currentState->nextInstructionAddress = memoryAddress;
    }    
    return 1;
}

int J6NP(struct mixState * currentState, struct mixInstruction * instruction){
    int allZero, i;
    allZero = 0;
    for (i = 1; i < 3 && allZero == 0; i++){
        if(currentState->rI[5][i] != 0){
            allZero = 1;
        }
    }
    if(currentState->rI[5][0] != 0 || allZero == 1){
        int memoryAddress;
        int J1, J2;
        memoryAddress = computeAddress(currentState, instruction);
        J1 = currentState->nextInstructionAddress/currentState->mixByteStates;
        J2 = currentState->nextInstructionAddress%currentState->mixByteStates;
        currentState->rJ[0] = J1;
        currentState->rJ[1] = J2;
        currentState->nextInstructionAddress = memoryAddress;
    }    
    return 1;
}

int registerJumpControl6(struct mixState * currentState, struct mixInstruction * instruction){
    if(instruction->F == 0){
        return J6N(currentState, instruction);
    }
    else if(instruction->F == 1){
        return J6Z(currentState, instruction);
    }
    else if(instruction->F == 2){
        return J6P(currentState, instruction);
    }
    else if(instruction->F == 3){
        return J6NN(currentState, instruction);
    }
    else if(instruction->F == 4){
        return J6NZ(currentState, instruction);
    }
    else if(instruction->F == 5){
        return J6NP(currentState, instruction);
    }
}

