#include "MixMisc.h"

int SLA (struct mixState * currentState, struct mixInstruction * instruction){
    int i, shiftCount;
    shiftCount = computeAddress(currentState, instruction);
    if(shiftCount > 0){
        shiftRegister(&currentState->rA[1], 5, shiftCount);
    }
    return 2;
}

int SRA (struct mixState * currentState, struct mixInstruction * instruction){
    int i, shiftCount;
    shiftCount = computeAddress(currentState, instruction);
    if(shiftCount > 0){
        shiftRegister(&currentState->rA[1], 5, -shiftCount);
    }
    return 2;
}

int SLAX (struct mixState * currentState, struct mixInstruction * instruction){
    int i, shiftCount;
    shiftCount = computeAddress(currentState, instruction);
    if(shiftCount > 0){
        int temp[10] = {0,0,0,0,0,0,0,0,0,0};
        for ( i = 0; i < 5; i++){
            temp[i] = currentState->rA[i + 1];
            temp[i+ 5] = currentState->rX[i + 1];
        }
        shiftRegister(temp, 10, shiftCount);
        for ( i = 0; i < 5; i++){
            currentState->rA[i + 1] = temp[i];
            currentState->rX[i + 1] = temp[i+5];
        }
    }
    return 2;
}

int SRAX (struct mixState * currentState, struct mixInstruction * instruction){
    int i, shiftCount;
    shiftCount = computeAddress(currentState, instruction);
    printf("%d\n", shiftCount);
    if(shiftCount > 0){
        int temp[10] = {0,0,0,0,0,0,0,0,0,0};
        for ( i = 0; i < 5; i++){
            temp[i] = currentState->rA[i + 1];
            temp[i+ 5] = currentState->rX[i + 1];
        }
        shiftRegister(temp, 10, -shiftCount);
        for ( i = 0; i < 5; i++){
            currentState->rA[i + 1] = temp[i];
            currentState->rX[i + 1] = temp[i+5];
        }
    }
    return 2;
}

int SLC (struct mixState * currentState, struct mixInstruction * instruction){
    int i, shiftCount;
    shiftCount = computeAddress(currentState, instruction);
    if(shiftCount > 0){
        shiftCount = shiftCount % 10;
        int temp[20] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
        for ( i = 0; i < 5; i++){
            temp[i] = currentState->rA[i + 1];
            temp[i+ 5] = currentState->rX[i + 1];
            temp[i + 10] = currentState->rA[i + 1];
            temp[i+ 15] = currentState->rX[i + 1];
        }
        shiftRegister(temp, 20, shiftCount);
        for ( i = 0; i < 5; i++){
            currentState->rA[i + 1] = temp[i];
            currentState->rX[i + 1] = temp[i+5];
        }
    }
    return 2;
}

int SRC (struct mixState * currentState, struct mixInstruction * instruction){
    int i, shiftCount;
    shiftCount = computeAddress(currentState, instruction);
    if(shiftCount > 0){
        shiftCount = shiftCount % 10;
        int temp[20] = {0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0};
        for ( i = 0; i < 5; i++){
            temp[i] = currentState->rA[i + 1];
            temp[i+ 5] = currentState->rX[i + 1];
            temp[i + 10] = currentState->rA[i + 1];
            temp[i+ 15] = currentState->rX[i + 1];
        }
        shiftRegister(temp, 20, -shiftCount);
        for ( i = 0; i < 5; i++){
            currentState->rA[i + 1] = temp[10 + i];
            currentState->rX[i + 1] = temp[i + 15];
        }
    }
    return 2;
}

int shiftControl(struct mixState * currentState, struct mixInstruction * instruction){
    if(instruction->F == 0){
        return SLA(currentState, instruction);
    }
    else if(instruction->F==1){
        return SRA(currentState, instruction);
    }
    else if(instruction->F==2){
        return SLAX(currentState, instruction);
    }
    else if(instruction->F==3){
        return SRAX(currentState, instruction);
    }
    else if(instruction->F==4){
        return SLC(currentState, instruction);
    }
    else if(instruction->F==5){
        return SRC(currentState, instruction);
    }
}

int MOVE(struct mixState * currentState, struct mixInstruction * instruction){
    int initialMemoryAddress, destinationMemoryAddress, i, j;
    initialMemoryAddress = computeAddress(currentState, instruction);
    struct mixInstruction tempInstruction;
    tempInstruction.S = 0;
    tempInstruction.A1 = 0;
    tempInstruction.A2 = 0;
    tempInstruction.I = 1;
    tempInstruction.F = 0;
    destinationMemoryAddress = computeAddress(currentState, &tempInstruction);
    if(initialMemoryAddress >=0 && destinationMemoryAddress >= 0){
        i = 0;
        while( i < instruction->F && initialMemoryAddress < 4000 && destinationMemoryAddress < 4000){
            for(j = 0; j < 6; j++){
               currentState->Mem[destinationMemoryAddress][j] = currentState->Mem[initialMemoryAddress][j];
            }
            i++;
            if(i<instruction->F){
                destinationMemoryAddress++;
                initialMemoryAddress++;
            }
        }
        currentState->rI[0][1] = destinationMemoryAddress / currentState->mixByteStates;
        currentState->rI[0][2] = destinationMemoryAddress % currentState->mixByteStates;
        if(initialMemoryAddress >= 4000 || destinationMemoryAddress > 4000){
            printf("Memory Failure\n");
        }
    }
    else{
        printf("Memory Failure\n");
    }
    
    return 1 + 2*instruction->F;
}

int NOP(struct mixState * currentState, struct mixInstruction * instruction){
    return 1;
}

int specialControl(struct mixState * currentState, struct mixInstruction * instruction){
    if(instruction->F == 0){
        return NUM(currentState, instruction);
    }
    else if(instruction->F==1){
        return CHAR(currentState, instruction);
    }
    else if(instruction->F==2){
        return HLT(currentState, instruction);
    }
}

int HLT(struct mixState * currentState, struct mixInstruction * instruction){
    currentState->running = 0;
    return 1;
}

int NUM(struct mixState * currentState, struct mixInstruction * instruction){
    int decimalNumber[10] = {0,0,0,0,0,0,0,0,0,0};
    int convertedNumber[5] = {0,0,0,0,0};
    int addedNumber[5] = {0,0,0,0,0};
    int addedValue;
    int overflow = 0;
    int i = 0;
    for(i = 0; i <5; i++){
        decimalNumber[i] = currentState->rA[i + 1] % 10;
        decimalNumber[i+5] = currentState->rX[i+1] % 10;
    }
    int j =0;
    for(i = 0; i < 5; i++) {
        addedValue = decimalNumber[2*i]*10 + decimalNumber[2*i + 1];
        for (j = 0; j <5; j++) {
            addedNumber[j] = 0;
        }
        addedNumber[4] = 1;
        multiplyWord(addedNumber, 5, currentState->mixByteStates, addedValue);
        overflow = multiplyWord(convertedNumber, 5, currentState->mixByteStates, 100) || overflow;
        overflow = addWords(convertedNumber, addedNumber, 5, currentState->mixByteStates) || overflow;
    }
    for (i = 0; i <5; i++){
        currentState->rA[i + 1] = convertedNumber[i];
    }
    if (overflow){
        currentState->overflow = 1;
    }
    return 1;
}

int CHAR(struct mixState * currentState, struct mixInstruction * instruction){
    int convertedNumber[10] = {0,0,0,0,0,0,0,0,0,0};
    int addedNumber[10] = {0,0,0,0,0,0,0,0,0,0};
    int addedValue;
    int i = 0;
    int j =0;
    for(i = 0; i < 5; i++) {
        addedValue = currentState->rA[i + 1];
        for (j = 0; j <10; j++) {
            addedNumber[j] = 0;
        }
        addedNumber[9] = 1;
        multiplyWord(addedNumber, 10, 10, addedValue);
        multiplyWord(convertedNumber, 10, 10, currentState->mixByteStates);
        addWords(convertedNumber, addedNumber, 10, 10);
    }
    for (i = 0; i <5; i++){
        currentState->rA[i + 1] = convertedNumber[i] + 30;
        currentState->rX[i + 1] = convertedNumber[5 + i] + 30;
    }
    return 1;
}