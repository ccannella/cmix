#ifndef MIXIO_H
#define MIXIO_H

#include "MixState.h"

int IN(struct mixState * currentState, struct mixInstruction * instruction);
int OUT(struct mixState * currentState, struct mixInstruction * instruction);
int IOC(struct mixState * currentState, struct mixInstruction * instruction);
int JRED(struct mixState * currentState, struct mixInstruction * instruction);
int JBUS(struct mixState * currentState, struct mixInstruction * instruction);

#endif
