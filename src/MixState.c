#include "MixState.h"

void initializeRegister(int registerName[], const int registerSize){
    int i;
    for (i = 0; i < registerSize; i++){
        registerName[i] = 0;
    }
};

void setByteInformation(int mixBase, int* states, int* length){
    int values, byteLength;
    values = 1;
    byteLength = 0;
    while ( values < 64 && values < 100){
        values *= mixBase;
        byteLength++;
    }
    if(values > 100) values = 100;  
    *states = values;
    *length = byteLength;
}

void initializeMemory(int *memory, const int memoryLength, const int wordLength){
    int i, j;
    for (i = 0; i < memoryLength; i++){
        for(j = 0; j < wordLength; j++){
            *(memory + i*wordLength + j) = 0;
        }
    }
}

void initializeIOPort(struct mixIOPort * ioPort, int blockSize) {
    ioPort->blockSize = blockSize;
    ioPort->busy = 0;
    ioPort->in = 0;
    ioPort->out = 0;
    ioPort->ioc = 0;
    ioPort->location = 0;
}

void initializeIOPorts(struct mixState * currentState) {
    int i;
    for (i = 0; i <16; i++){
        initializeIOPort(&currentState->ioPorts[i], 100);
    }
    for (i = 16; i <18; i++){
        initializeIOPort(&currentState->ioPorts[i], 16);
    }
    for (i = 18; i <19; i++){
        initializeIOPort(&currentState->ioPorts[i], 24);
    }
    for (i = 19; i <21; i++){
        initializeIOPort(&currentState->ioPorts[i], 14);
    }
}

void initializeState(struct mixState *currentState){
    setByteInformation(currentState->mixBase, &currentState->mixByteStates, &currentState->mixByteLength);
    printf ("Byte Length - %d\nByte Values - %d\n", currentState->mixByteLength, currentState->mixByteStates);
   
    printf("Beginning Initialization\n");
    initializeRegister(currentState->rA, 6);
    initializeRegister(currentState->rX, 6);
    initializeMemory(&currentState->rI[0][0], 6, 3);
    initializeRegister(currentState->rJ, 2);
    initializeMemory(&currentState->Mem[0][0], 4000, 6);
    initializeIOPorts(currentState);
    currentState->overflow = 0;
    currentState->comparison = 0;
    currentState->nextInstructionAddress = 0;
    currentState->running = 0;
    currentState->currentInstruction.S = 0;
    currentState->currentInstruction.A1 = 0;
    currentState->currentInstruction.A2 = 0;
    currentState->currentInstruction.I = 0;
    currentState->currentInstruction.F = 0;
    printf("Initialization Complete!\n");
    
}

void printRegister(int registerName[], const int registerSize){
    int i;
    printf("|");
    if(registerSize > 2){
        printf("%c|", (registerName[0] ==0)? '+' : '-');
    }
    else{
        printf("+|%d|", registerName[0]);
    }
    for (i = 1; i < registerSize; i ++){
        printf("%d|", registerName[i]);
    }
    printf("\n");
};

int computeAddress(struct mixState * currentState, struct mixInstruction * instruction){
    int memoryAddress;
    memoryAddress = (instruction->S==0)? instruction->A1 * currentState->mixByteStates + instruction->A2 : -instruction->A1*currentState->mixByteStates - instruction->A2;
    if(instruction->I > 0 && instruction->I < 7){
        memoryAddress += (currentState-> rI[instruction->I-1][0] == 0)? currentState-> rI[instruction->I-1][1] * currentState->mixByteStates + currentState-> rI[instruction->I-1][2] : -currentState-> rI[instruction->I-1][1] * currentState->mixByteStates - currentState-> rI[instruction->I-1][2];
    }
    return memoryAddress;
}


void loadRegister(struct mixState *currentState, int registerName[6], struct mixInstruction * instruction){
    int lowerField, upperField, memoryAddress, i;
    lowerField = instruction->F/8;
    upperField = instruction->F%8;
    if(upperField >= lowerField && upperField <= 5 && lowerField <=5){
        registerName[0] = 0;
        memoryAddress = computeAddress(currentState, instruction);
        for( i = 1; i <= 5 - (upperField- lowerField); i++){
            registerName[i] = 0;
        }
        if(lowerField == 0){
            registerName[0] = currentState->Mem[memoryAddress][0];
            lowerField = 1;
        }

        for(; lowerField <= upperField; lowerField++){
            
            registerName[5 - (upperField - lowerField)] = currentState->Mem[memoryAddress][lowerField];
        }
    }
}

void fieldMask(struct mixState * currentState, int destination[6], int initial[6], int F){
    int lowerField, upperField, memoryAddress, i;
    lowerField = F/8;
    upperField = F%8;
    if(upperField >= lowerField && upperField <= 5 && lowerField <=5){
        destination[0] = 0;
        for( i = 1; i <= 5 - (upperField- lowerField); i++){
            destination[i] = 0;
        }
        if(lowerField == 0){
            destination[0] = initial[0];
            lowerField = 1;
        }

        for(; lowerField <= upperField; lowerField++){
            
            destination[5 - (upperField - lowerField)] = initial[lowerField];
        }
    }
}

void copyRegister(struct mixState *currentState, int registerName[6], struct mixInstruction * instruction){
    int lowerField, upperField, memoryAddress, i;
    lowerField = instruction->F/8;
    upperField = instruction->F%8;
    if(upperField >= lowerField && upperField <= 5 && lowerField <=5){
        memoryAddress =computeAddress(currentState, instruction);
        if(lowerField == 0){
            currentState->Mem[memoryAddress][0] = registerName[0];
            lowerField = 1;
        }
        for(; lowerField <= upperField; lowerField++){
            
            currentState->Mem[memoryAddress][lowerField] = registerName[5 - (upperField - lowerField)];
        }
    }
}

int cmpWord(int word1[], int word2[], int wordSize){
    int i, largestValue, largestIndex,comparison;
    largestValue = 0;
    largestIndex = 0;
    for (i = 0; i < wordSize && largestValue == 0; i++){
        if(word1[i] > largestValue){
            largestValue = word1[i];
            largestIndex = i;
        }
        if(word2[i] > largestValue){
            largestValue = word2[i];
            largestIndex = i;
        }
        else if(word2[i] == largestValue){
            largestValue = 0;
            largestIndex = 0;
        }
    }
    if(largestValue == 0){
        comparison = 0;
    }
    else if(word1[largestIndex] == largestValue){
        comparison = 1;
    }
    else{
        comparison = -1;
    }
    return comparison;
}
int multiplyWord(int word[], int wordSize, int wordBase, int multiplicationValue){
    int i;
    for (i = 0; i < wordSize; i++){
        word[i] *= multiplicationValue;
    }
    for (i =wordSize -1; i >0; i--){
        if(word[i] > wordBase){
            word[i-1] += word[i] / wordBase;
            word[i] = word[i] %wordBase;
        }
    } 
    if(word[0] > wordBase){
        word[0] = word[0] %wordBase;
        return 1;
    }
    return 0;
}

int addWords(int word1[], int word2[], int wordSize, int wordBase){
    int i;
    for(i =0; i < wordSize; i++){
        word1[i] += word2[i];
    }
    for (i =wordSize -1; i >0; i--){
        if(word1[i] > wordBase){
            word1[i-1] += word1[i] / wordBase;
            word1[i] = word1[i] %wordBase;
        }
    }
    if(word1[0] > wordBase){
        word1[0] = word1[0] %wordBase;
        return 1;
    }
}

int subtractWords(int word1[], int word2[], int wordSize, int wordBase){
    int i;
    for(i =0; i < wordSize; i++){
        word1[i] -= word2[i];
    }
    for ( i = wordSize - 1; i >0 ; i--){
        if(word1[i] < 0){
            word1[i-1] --;
            word1[i] += wordBase;
        }
    }
}

void entranceFunction(struct mixState * currentState, struct mixInstruction * instruction, int registerName[], int registerSize){
    int memoryAddress;
    int i, D1, D2;
    memoryAddress = computeAddress(currentState, instruction);
    for(i = 1; i<6; i++){
        registerName[0] = 0;
    }
    if(memoryAddress == 0){
        registerName[0] = instruction->S;

    }
    else{
        registerName[0] = (memoryAddress>0)? 0:1;
        memoryAddress = (memoryAddress >0)? memoryAddress: -memoryAddress;
        D1 = memoryAddress/currentState->mixByteStates;
        D2 = memoryAddress%currentState->mixByteStates;
        registerName[registerSize - 2] = D1;
        registerName[registerSize - 1] = D2;
    }    
}

void incrementFunction(struct mixState * currentState, struct mixInstruction * instruction, int registerName[], int registerSize){
    int memoryAddress;
    int i, D1, D2, largestIndex, sign;
    memoryAddress = computeAddress(currentState, instruction);
    if(memoryAddress != 0){
        sign = (memoryAddress > 0) ? 1 : -1;
        memoryAddress *= sign;
        D1 = memoryAddress/currentState->mixByteStates;
        D2 = memoryAddress%currentState->mixByteStates;
        memoryAddress *= sign;
        D1 = (memoryAddress >0)? D1:-D1;
        D2 = (memoryAddress > 0)?D2:-D2;
        for(i = 1; i<registerSize; i++){
            registerName[i] = (registerName[0] == 0)? registerName[i] : -registerName[i];
        }
        registerName[registerSize - 2] += D1;
        registerName[registerSize - 1] += D2;
        largestIndex = 0;
        for(i = 1; i < registerSize && largestIndex == 0; i++){
            if(registerName[i] !=0){
                largestIndex = i;
            }
        }
        if(largestIndex != 0){
            registerName[0] = (registerName[largestIndex] < 0) ? 1 : 0;
            for (i = 1; i < registerSize; i++){
                registerName[i] = (registerName[0] == 0) ? registerName[i] : -registerName[i];
            }
            for(i = registerSize - 1; i > 1; i--){
                if(registerName[i] >= currentState->mixByteStates){
                    registerName[i -1] += registerName[i]/currentState->mixByteStates;
                    registerName[i] = registerName[i]%currentState->mixByteStates;
                }
                else if(registerName[i] < 0){
                    registerName[i - 1] -= 1;
                    registerName[i] += currentState->mixByteStates;
                }
            }
            if(registerName[1] > currentState->mixByteStates){
                if(registerSize == 6){
                    registerName[1] = registerName[1] %currentState->mixByteStates;
                    currentState->overflow = 1;
                }
                else{
                    printf("Undefined Action: Overflow of Index Register via Increment Operator\n");
                    registerName[1] = registerName[1]%currentState->mixByteStates;
                }
            }
        }
        
    }
}

void decrementFunction(struct mixState * currentState, struct mixInstruction * instruction, int registerName[], int registerSize){
    int memoryAddress;
    int i, D1, D2, largestIndex, sign;
    memoryAddress = -computeAddress(currentState, instruction);
    if(memoryAddress != 0){
        sign = (memoryAddress > 0) ? 1 : -1;
        memoryAddress *= sign;
        D1 = memoryAddress/currentState->mixByteStates;
        D2 = memoryAddress%currentState->mixByteStates;
        memoryAddress *= sign;
        D1 = (memoryAddress >0)? D1:-D1;
        D2 = (memoryAddress > 0)?D2:-D2;
        for(i = 1; i<registerSize; i++){
            registerName[i] = (registerName[0] == 0)? registerName[i] : -registerName[i];
        }
        registerName[registerSize - 2] += D1;
        registerName[registerSize - 1] += D2;
        largestIndex = 0;
        for(i = 1; i < registerSize && largestIndex == 0; i++){
            if(registerName[i] !=0){
                largestIndex = i;
            }
        }
        if(largestIndex != 0){
            registerName[0] = (registerName[largestIndex] < 0) ? 1 : 0;
            for (i = 1; i < registerSize; i++){
                registerName[i] = (registerName[0] == 0) ? registerName[i] : -registerName[i];
            }
            for(i = registerSize - 1; i > 1; i--){
                if(registerName[i] >= currentState->mixByteStates){
                    registerName[i -1] += registerName[i]/currentState->mixByteStates;
                    registerName[i] = registerName[i]%currentState->mixByteStates;
                }
                else if(registerName[i] < 0){
                    registerName[i - 1] -= 1;
                    registerName[i] += currentState->mixByteStates;
                }
            }
            if(registerName[1] > currentState->mixByteStates){
                if(registerSize == 6){
                    registerName[1] = registerName[1] %currentState->mixByteStates;
                    currentState->overflow = 1;
                }
                else{
                    printf("Undefined Action: Overflow of Index Register via Increment Operator\n");
                    registerName[1] = registerName[1]%currentState->mixByteStates;
                }
            }
        }
        
    }
}

void registerComparison(struct mixState * currentState, struct mixInstruction * instruction, int registerName[], int registerSize){
    int registerCopy[6] = {0,0,0,0,0,0};
    int tempRegister[6] = {0,0,0,0,0,0};
    int tempMemory[6] = {0,0,0,0,0,0};
    int i, allZero, memoryAddress, comparison;
    if(registerSize == 6){
        for (i = 0; i < 6; i++){
            registerCopy[i] = registerName[i];
        }
    }
    else{
        registerCopy[0] = registerName[0];
        registerCopy[4] = registerName[1];
        registerCopy[5] = registerName[2];
    }
    memoryAddress = computeAddress(currentState, instruction);
    fieldMask(currentState, tempRegister, registerCopy, instruction->F);
    fieldMask(currentState, tempMemory, &currentState->Mem[memoryAddress][0], instruction->F);
    printRegister(tempRegister, 6);
    printRegister(tempMemory, 6);
    allZero = 0;
    for (i = 1; i < 6 && allZero == 0; i++){
        if((tempRegister[i] != 0) || (tempMemory[i] != 0)){
            allZero = 1;
        }
    }
    comparison = cmpWord(&tempRegister[1], &tempMemory[1], 5);
    if(allZero == 0){
        currentState->comparison = 0;
    }
    else if(comparison != 0){
        if(tempRegister[0] == tempMemory[0]){
            currentState->comparison = (tempRegister[0] == 0)? comparison: -comparison;
        }
        else{
            currentState->comparison = (tempRegister[0] == 0)? 1: -1;
        }
    }
    else if(tempRegister[0] == tempMemory[0]){
        currentState->comparison = 0;
    }
    else{
        currentState->comparison = (tempRegister[0] == 0)? 1: -1;
    }
    
}

void shiftRegister(int registerName[], int registerSize, int shiftCount){
    int i;
    if(shiftCount < registerSize && shiftCount >= 0){
        for(i = 0; i < registerSize; i++){
            if(i < registerSize - shiftCount){
                registerName[i] = registerName[i + shiftCount];
            }
            else{
                registerName[i] = 0;
            }
        }
    }
    else if(shiftCount > -registerSize && shiftCount < 0){
        for(i = registerSize - 1; i >= 0; i--){
            if(i > -shiftCount - 1){
                registerName[i] = registerName[i + shiftCount];
            }
            else{
                registerName[i] = 0;
            }
        }        
    }
    else{
        for (i = 0; i < registerSize; i++){
            registerName[i] = 0;
        }
    }
}

