#include "MixState.h"
#include "MixLoad.h"
#include "MixStore.h"
#include "MixArith.h"
#include "MixTransfer.h"
#include "MixComp.h"
#include "MixJump.h"
#include "MixMisc.h"
#include "MixIO.h"

#include "MixDecoder.h"

int main(){
    struct mixState currentState;
    currentState.mixBase = 10;
    initializeState(&currentState);

    currentState.rI[0][0] = 0;
    currentState.rI[0][1] = 10;
    currentState.rI[0][2] = 1;
    currentState.Mem[1000][0] = 0;
    currentState.Mem[1000][1] = 1;
    currentState.Mem[1000][2] = 2;
    currentState.Mem[1000][3] = 3;
    currentState.Mem[1000][4] = 4;
    currentState.Mem[1000][5] = 5;
    currentState.Mem[1001][0] = 0;
    currentState.Mem[1002][1] = 0;
    currentState.Mem[1003][2] = 0;
    currentState.Mem[1004][3] = 0;
    currentState.Mem[1005][4] = 0;
    currentState.Mem[1006][5] = 0;
    currentState.rA[0] = 1;
    currentState.rA[1] = 0;
    currentState.rA[2] = 0;
    currentState.rA[3] = 31;
    currentState.rA[4] = 32;
    currentState.rA[5] = 39;
    currentState.rX[0] = 0;
    currentState.rX[1] = 37;
    currentState.rX[2] = 57;
    currentState.rX[3] = 47;
    currentState.rX[4] = 30;
    currentState.rX[5] = 30;
    printRegister(&currentState.rA[0], 6);
    printRegister(&currentState.rX[0], 6);
    struct mixInstruction testInstruction;
    testInstruction.S = 0;
    testInstruction.A1 = 10;
    testInstruction.A2 = 0;
    testInstruction.I = 0;
    testInstruction.F = 0;
    testInstruction.C = 5;

    struct mixDecoder decoder;
    initializeDecoder(&decoder, 64, standardDecoder);

    (*decoder.instructionSet[5])(&currentState, &testInstruction);
    printRegister(&currentState.rA[0], 6);
    printRegister(&currentState.rX[0], 6);

    testInstruction.S = 0;
    testInstruction.A1 = 0;
    testInstruction.A2 = 1;
    testInstruction.I = 0;
    testInstruction.F = 0;
    testInstruction.C = 48;
    (*decoder.instructionSet[48])(&currentState, &testInstruction);
    printRegister(&currentState.rA[0], 6);
    printRegister(&currentState.rX[0], 6);

    testInstruction.S = 0;
    testInstruction.A1 = 10;
    testInstruction.A2 = 0;
    testInstruction.I = 0;
    testInstruction.F = 1;
    testInstruction.C = 5;
    (*decoder.instructionSet[5])(&currentState, &testInstruction);
    printRegister(&currentState.rA[0], 6);
    printRegister(&currentState.rX[0], 6);
    char c[80];
    scanf("%c", c);
    return 0;
}

