#include "MixTransfer.h"

int ENTA(struct mixState * currentState, struct mixInstruction * instruction){
    entranceFunction(currentState, instruction, currentState->rA, 6);
    return 1;
}

int ENTX(struct mixState * currentState, struct mixInstruction * instruction){
    entranceFunction(currentState, instruction, currentState->rX, 6);
    return 1;
}

int ENT1(struct mixState * currentState, struct mixInstruction * instruction){
    entranceFunction(currentState, instruction, &currentState->rI[0][0], 3);
    return 1;
}

int ENT2(struct mixState * currentState, struct mixInstruction * instruction){
    entranceFunction(currentState, instruction, &currentState->rI[1][0], 3);
    return 1;
}

int ENT3(struct mixState * currentState, struct mixInstruction * instruction){
    entranceFunction(currentState, instruction, &currentState->rI[2][0], 3);
    return 1;
}

int ENT4(struct mixState * currentState, struct mixInstruction * instruction){
    entranceFunction(currentState, instruction, &currentState->rI[3][0], 3);
    return 1;
}

int ENT5(struct mixState * currentState, struct mixInstruction * instruction){
    entranceFunction(currentState, instruction, &currentState->rI[4][0], 3);
    return 1;
}

int ENT6(struct mixState * currentState, struct mixInstruction * instruction){
    entranceFunction(currentState, instruction, &currentState->rI[5][0], 3);
    return 1;
}

int ENNA(struct mixState * currentState, struct mixInstruction * instruction){
    entranceFunction(currentState, instruction, currentState->rA, 6);
    currentState->rA[0] = (currentState->rA[0] == 0)? 1:0;
    return 1;
}

int ENNX(struct mixState * currentState, struct mixInstruction * instruction){
    entranceFunction(currentState, instruction, currentState->rX, 6);
    currentState->rX[0] = (currentState->rX[0] == 0)? 1:0;
    return 1;
}

int ENN1(struct mixState * currentState, struct mixInstruction * instruction){
    entranceFunction(currentState, instruction, &currentState->rI[0][0], 3);
    currentState->rI[0][0] = (currentState->rI[0][0] == 0)? 1:0;
    return 1;
}

int ENN2(struct mixState * currentState, struct mixInstruction * instruction){
    entranceFunction(currentState, instruction, &currentState->rI[1][0], 3);
    currentState->rI[1][0] = (currentState->rI[2][0] == 0)? 1:0;
    return 1;
}

int ENN3(struct mixState * currentState, struct mixInstruction * instruction){
    entranceFunction(currentState, instruction, &currentState->rI[2][0], 3);
    currentState->rI[2][0] = (currentState->rI[2][0] == 0)? 1:0;
    return 1;
}

int ENN4(struct mixState * currentState, struct mixInstruction * instruction){
    entranceFunction(currentState, instruction, &currentState->rI[3][0], 3);
    currentState->rI[3][0] = (currentState->rI[3][0] == 0)? 1:0;
    return 1;
}

int ENN5(struct mixState * currentState, struct mixInstruction * instruction){
    entranceFunction(currentState, instruction, &currentState->rI[4][0], 3);
    currentState->rI[4][0] = (currentState->rI[4][0] == 0)? 1:0;
    return 1;
}

int ENN6(struct mixState * currentState, struct mixInstruction * instruction){
    entranceFunction(currentState, instruction, &currentState->rI[5][0], 3);
    currentState->rI[6][0] = (currentState->rI[6][0] == 0)? 1:0;
    return 1;
}

int INCA(struct mixState * currentState, struct mixInstruction * instruction){
    incrementFunction(currentState, instruction, currentState->rA, 6);
    return 1;
}

int INCX(struct mixState * currentState, struct mixInstruction * instruction){
    incrementFunction(currentState, instruction, currentState->rX, 6);
    return 1;
}

int INC1(struct mixState * currentState, struct mixInstruction * instruction){
    incrementFunction(currentState, instruction, &currentState->rI[0][0], 3);
    return 1;
}

int INC2(struct mixState * currentState, struct mixInstruction * instruction){
    incrementFunction(currentState, instruction, &currentState->rI[1][0], 3);
    return 1;
}

int INC3(struct mixState * currentState, struct mixInstruction * instruction){
    incrementFunction(currentState, instruction, &currentState->rI[2][0], 3);
    return 1;
}

int INC4(struct mixState * currentState, struct mixInstruction * instruction){
    incrementFunction(currentState, instruction, &currentState->rI[3][0], 3);
    return 1;
}

int INC5(struct mixState * currentState, struct mixInstruction * instruction){
    incrementFunction(currentState, instruction, &currentState->rI[4][0], 3);
    return 1;
}

int INC6(struct mixState * currentState, struct mixInstruction * instruction){
    incrementFunction(currentState, instruction, &currentState->rI[5][0], 3);
    return 1;
}

int DECA(struct mixState * currentState, struct mixInstruction * instruction){
    decrementFunction(currentState, instruction, currentState->rA, 6);
    return 1;
}

int DECX(struct mixState * currentState, struct mixInstruction * instruction){
    decrementFunction(currentState, instruction, currentState->rX, 6);
    return 1;
}

int DEC1(struct mixState * currentState, struct mixInstruction * instruction){
    decrementFunction(currentState, instruction, &currentState->rI[0][0], 3);
    return 1;
}

int DEC2(struct mixState * currentState, struct mixInstruction * instruction){
    decrementFunction(currentState, instruction, &currentState->rI[1][0], 3);
    return 1;
}

int DEC3(struct mixState * currentState, struct mixInstruction * instruction){
    decrementFunction(currentState, instruction, &currentState->rI[2][0], 3);
    return 1;
}

int DEC4(struct mixState * currentState, struct mixInstruction * instruction){
    decrementFunction(currentState, instruction, &currentState->rI[3][0], 3);
    return 1;
}

int DEC5(struct mixState * currentState, struct mixInstruction * instruction){
    decrementFunction(currentState, instruction, &currentState->rI[4][0], 3);
    return 1;
}

int DEC6(struct mixState * currentState, struct mixInstruction * instruction){
    decrementFunction(currentState, instruction, &currentState->rI[5][0], 3);
    return 1;
}

int addressTransferControlA(struct mixState * currentState, struct mixInstruction * instruction){
    if(instruction->F == 0){
        return INCA(currentState, instruction);
    }
    else if(instruction->F == 1){
        return DECA(currentState, instruction);
    }
    else if(instruction->F == 2){
        return ENTA(currentState, instruction);
    }
    else if(instruction->F == 3){
        return ENNA(currentState, instruction);
    }   
}

int addressTransferControlX(struct mixState * currentState, struct mixInstruction * instruction){
    if(instruction->F == 0){
        return INCX(currentState, instruction);
    }
    else if(instruction->F == 1){
        return DECX(currentState, instruction);
    }
    else if(instruction->F == 2){
        return ENTX(currentState, instruction);
    }
    else if(instruction->F == 3){
        return ENNX(currentState, instruction);
    }   
}

int addressTransferControlI1(struct mixState * currentState, struct mixInstruction * instruction){
    if(instruction->F == 0){
        return INC1(currentState, instruction);
    }
    else if(instruction->F == 1){
        return DEC1(currentState, instruction);
    }
    else if(instruction->F == 2){
        return ENT1(currentState, instruction);
    }
    else if(instruction->F == 3){
        return ENN1(currentState, instruction);
    }   
}

int addressTransferControlI2(struct mixState * currentState, struct mixInstruction * instruction){
    if(instruction->F == 0){
        return INC2(currentState, instruction);
    }
    else if(instruction->F == 1){
        return DEC2(currentState, instruction);
    }
    else if(instruction->F == 2){
        return ENT2(currentState, instruction);
    }
    else if(instruction->F == 3){
        return ENN2(currentState, instruction);
    }   
}

int addressTransferControlI3(struct mixState * currentState, struct mixInstruction * instruction){
    if(instruction->F == 0){
        return INC3(currentState, instruction);
    }
    else if(instruction->F == 1){
        return DEC3(currentState, instruction);
    }
    else if(instruction->F == 2){
        return ENT3(currentState, instruction);
    }
    else if(instruction->F == 3){
        return ENN3(currentState, instruction);
    }   
}

int addressTransferControlI4(struct mixState * currentState, struct mixInstruction * instruction){
    if(instruction->F == 0){
        return INC4(currentState, instruction);
    }
    else if(instruction->F == 1){
        return DEC4(currentState, instruction);
    }
    else if(instruction->F == 2){
        return ENT4(currentState, instruction);
    }
    else if(instruction->F == 3){
        return ENN4(currentState, instruction);
    }   
}

int addressTransferControlI5(struct mixState * currentState, struct mixInstruction * instruction){
    if(instruction->F == 0){
        return INC5(currentState, instruction);
    }
    else if(instruction->F == 1){
        return DEC5(currentState, instruction);
    }
    else if(instruction->F == 2){
        return ENT5(currentState, instruction);
    }
    else if(instruction->F == 3){
        return ENN5(currentState, instruction);
    }   
}

int addressTransferControlI6(struct mixState * currentState, struct mixInstruction * instruction){
    if(instruction->F == 0){
        return INC6(currentState, instruction);
    }
    else if(instruction->F == 1){
        return DEC6(currentState, instruction);
    }
    else if(instruction->F == 2){
        return ENT6(currentState, instruction);
    }
    else if(instruction->F == 3){
        return ENN6(currentState, instruction);
    }   
}

