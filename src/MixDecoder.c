#include "MixDecoder.h"

void initializeDecoder(struct mixDecoder * decoder, int instructionSetSize, void (*decoderSetup)(int (**instructionSet)(struct mixState *, struct mixInstruction *))) {
    decoder->instructionSetSize = instructionSetSize;
    decoder->instructionSet = malloc(instructionSetSize * sizeof(*decoder->instructionSet));
    decoderSetup(decoder->instructionSet);
};

void standardDecoder(int (**instructionSet)(struct mixState *, struct mixInstruction *)) {
    instructionSet[0] = NOP;
    instructionSet[1] = ADD;
    instructionSet[2] = SUB;
    instructionSet[3] = MUL;
    instructionSet[4] = DIV;
    instructionSet[5] = specialControl;
    instructionSet[6] = shiftControl;
    instructionSet[7] = MOVE;
    instructionSet[8] = LDA;
    instructionSet[9] = LD1;
    instructionSet[10] = LD2;
    instructionSet[11] = LD3;
    instructionSet[12] = LD4;
    instructionSet[13] = LD5;
    instructionSet[14] = LD6;
    instructionSet[15] = LDX;
    instructionSet[16] = LDAN;
    instructionSet[17] = LD1N;
    instructionSet[18] = LD2N;
    instructionSet[19] = LD3N;
    instructionSet[20] = LD4N;
    instructionSet[21] = LD5N;
    instructionSet[22] = LD6N;
    instructionSet[23] = LDXN;
    instructionSet[24] = STA;
    instructionSet[25] = ST1;
    instructionSet[26] = ST2;
    instructionSet[27] = ST3;
    instructionSet[28] = ST4;
    instructionSet[29] = ST5;
    instructionSet[30] = ST6;
    instructionSet[31] = STX;
    instructionSet[32] = STJ;
    instructionSet[33] = STZ;
    instructionSet[34] = JBUS;
    instructionSet[35] = IOC;
    instructionSet[36] = IN;
    instructionSet[37] = OUT;
    instructionSet[38] = JRED;
    instructionSet[39] = typicalJumpControl;
    instructionSet[40] = registerJumpControlA;
    instructionSet[41] = registerJumpControl1;
    instructionSet[42] = registerJumpControl2;
    instructionSet[43] = registerJumpControl3;
    instructionSet[44] = registerJumpControl4;
    instructionSet[45] = registerJumpControl5;
    instructionSet[46] = registerJumpControl6;
    instructionSet[47] = registerJumpControlX;
    instructionSet[48] = addressTransferControlA;
    instructionSet[49] = addressTransferControlI1;
    instructionSet[50] = addressTransferControlI2;
    instructionSet[51] = addressTransferControlI3;
    instructionSet[52] = addressTransferControlI4;
    instructionSet[53] = addressTransferControlI5;
    instructionSet[54] = addressTransferControlI6;
    instructionSet[55] = addressTransferControlX;
    instructionSet[56] = CMPA;
    instructionSet[57] = CMP1;
    instructionSet[58] = CMP2;
    instructionSet[59] = CMP3;
    instructionSet[60] = CMP4;
    instructionSet[61] = CMP5;
    instructionSet[62] = CMP6;
    instructionSet[63] = CMPX;
}
