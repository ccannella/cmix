#ifndef MIXSTATE_H
#define MIXSTATE_H

#include <stdio.h>
#include "MixInstruction.h"

struct mixIOPort{
    int blockSize;
    int busy;
    int in;
    int out;
    int ioc;
    int location;
};

struct mixState{
    int mixBase;
    int mixByteStates;
    int mixByteLength;
    int rA[6], rX[6], rI[6][3], rJ[2];
    int Mem[4000][6];
    int overflow;
    int comparison;
    int nextInstructionAddress;
    struct mixInstruction currentInstruction;
    struct mixIOPort ioPorts[21];
    int running;
};

void setByteInformation(int mixBase, int* states, int* length);
void initializeRegister(int registerName[], const int registerSize);
void initializeMemory(int *memory, const int memoryLength, const int wordLength);
void initializeIOPort(struct mixIOPort * ioPort, int blockSize);
void initializeIOPorts(struct mixState * currentState);
void initializeState(struct mixState *currentState);
void printRegister(int registerName[], const int registerSize);
void loadRegister(struct mixState * currentState, int registerName[6], struct mixInstruction * instruction);
int computeAddress(struct mixState * currentState, struct mixInstruction * instruction);
void copyRegister(struct mixState * currentState, int registerName[6], struct mixInstruction * instruction);
int cmpWord(int word1[], int word2[], int wordSize);
int multiplyWord(int word[], int wordSize, int wordBase, int multiplicationValue);
int addWords(int word1[], int word2[], int wordSize, int wordBase);
int subtractWords(int word1[], int word2[], int wordSize, int wordBase);
void entranceFunction(struct mixState * currentState, struct mixInstruction * instruction, int registerName[], int registerSize);
void incrementFunction(struct mixState * currentState, struct mixInstruction * instruction, int registerName[], int registerSize);
void decrementFunction(struct mixState * currentState, struct mixInstruction * instruction, int registerName[], int registerSize);
void fieldMask(struct mixState * currentState, int destination[6], int initial[6], int F);
void registerComparison(struct mixState * currentState, struct mixInstruction * instruction, int registerName[], int registerSize);
void shiftRegister(int registerName[], int registerSize, int shiftCount);

#endif
